# Setup #


## After cloning the repository, run the following commands in the project's root folder: ##
Install dependencies needed for laravel to work:
```
composer install
```

Install front-end dependencies + run asset compilation pipeline
```
npm install
bower install
gulp
```

Copy the example .env file to a new .env file
Edit the database configuration in this file to reflect your local database credentials
```
cp .env.example .env
```

Create an application key (used for password hashing):
```
php artisan key:generate
```

Migrate the database structure:
```
php artisan migrate
```

Seed the database with dummy data:
```
php artisan db:seed
```