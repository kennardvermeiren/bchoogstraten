<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Event extends Model
{
    protected $table = 'events';

    public $timestamps = true;

    protected $fillable = ['title', 'body', 'begin', 'end', 'location', 'active'];

    protected $dates = ['created_at', 'updated_at', 'begin', 'end'];

    /**
	 * Get the post's author.
	 *
	 * @return User
	 */
	public function user()
	{
		return $this->belongsTo('App\User');
	}

	public function edited() {
		if ($this->attributes['created_at'] != $this->attributes['updated_at']) {
			return true;
		}
		return false;
	}

	public function setTitleAttribute($value)
	{
		$this->attributes['title'] = $value;

		if (! $this->exists) {
			$this->attributes['slug'] = str_slug($value);
		}
	}

	public function setBeginAttribute($value) {
		$this->attributes['begin'] = Carbon::createFromFormat('Y-m-d*H:i', $value, 'Europe/Brussels');//->tz('UTC');
	}

	public function getBeginHTML5() {
		$c = new Carbon($this->attributes['begin']);
		return $c->format('Y-m-d\TH:i');
	}

	public function setEndAttribute($value) {
		$this->attributes['end'] = Carbon::createFromFormat('Y-m-d*H:i', $value, 'Europe/Brussels');//->tz('UTC');
	}

	public function getEndHTML5() {
		$c = new Carbon($this->attributes['end']);
		return $c->format('Y-m-d\TH:i');
	}
}
