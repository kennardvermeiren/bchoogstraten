<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Image;

class Album extends Model
{
    protected $table = 'albums';

    protected $fillable = ['title', 'summary', 'body', 'active'];

    public $timestamps = true;

    /**
	 * Get the post's author.
	 *
	 * @return User
	 */
	public function user()
	{
		return $this->belongsTo('App\User');
	}

	public function edited() {
		if ($this->attributes['created_at'] != $this->attributes['updated_at']) {
			return true;
		}
		return false;
	}

    public function setTitleAttribute($value)
	{
		$this->attributes['title'] = $value;

		if (! $this->exists) {
			$this->attributes['slug'] = str_slug($value);
		}
	}

	public function setSummaryAttribute($value)
	{
        $tmp = preg_replace('/\s+/u', '', html_entity_decode(strip_tags($value)));
        if (strlen($tmp) == 0) {
            $this->attributes['summary'] = "";
        } else {
        	$this->attributes['summary'] = $value;
        }
	}

	public function getSummaryAttribute($value) {
		if (strlen($value) == 0) {
            return str_limit($this->body, 200);
        }
		return $value;
	}

	public function images() {
    	return $this->hasMany('App\Image');
    }
}
