<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Post;
use App\Event;
use Carbon\Carbon;
use Illuminate\Support\Str;

class BlogController extends Controller
{
    public function index()
    {
        $posts = Post::where('active', 1)
            ->orderBy('updated_at', 'desc')
            ->paginate(config('blog.posts_per_page'));

        $event = Event::where('active', 1)->orderBy('begin', 'asc')->first();

        return view('site.index', compact('posts', 'event'));
    }

    public function showPost($slug)
    {
        $post = Post::whereSlug($slug)->firstOrFail();

        return view('site.post')->withPost($post);
    }
}
