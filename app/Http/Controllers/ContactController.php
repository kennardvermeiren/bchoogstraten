<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\SendContactMessageRequest;

class ContactController extends Controller
{
    public function getContact() {
        return \View::make('site/contact-us');
    }

    public function sendMail(SendContactMessageRequest $request) {
        $data = $request->except(['g-recaptcha-response']);

        if (\Mail::send(
            'emails.contact', 
            array(
                'email' => $request->get('email'),
                'msg' => $request->get('message')
                ), 
            function ($message) use ($data) {
                $message
                    ->from($data['email'], $data['email'])
                    ->to('contact@bchoogstraten.be', 'BCHoogstraten Contact')
                    ->subject('Contact BCHoogstraten');
            }
        )) {
            return \Redirect::to('contact')->with('success', trans('contact.alerts.message_sent'));
        }

        
        //return Redirect::to('contact-us')->with('error', Lang::get('contact/contact.alerts.message_failed'));
        return \Redirect::to('contact-us')
        ->withErrors($validator)
        ->withInput();
    }
}
