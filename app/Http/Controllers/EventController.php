<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Event;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::where('active', 1)
            ->orderBy('begin', 'asc')
            ->paginate(config('calendar.events_per_page'));

        return view('site.calendar', compact('events'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showEvent($slug)
    {
        $event = Event::whereSlug($slug)->firstOrFail();

        return view('site.event')->withEvent($event);
    }
}
