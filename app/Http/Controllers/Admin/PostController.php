<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Post;
use File;
use Auth;

use App\Http\Requests\Post\CreatePostRequest;
use App\Http\Requests\Post\UpdatePostRequest;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $posts = Post::orderBy('updated_at', 'desc')->paginate(15);
        
        return view('admin.posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.posts.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(CreatePostRequest $request)
    {
        $post = new Post;
        $post->fill($request->all());
        $post->user()->associate(Auth::user());
        
        $post->save();

        return redirect('admin/posts')->with('success', 'Post created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $post = Post::findOrFail($id);
        return view('admin.posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        return view('admin.posts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, UpdatePostRequest $request)
    {
        $post = Post::findOrFail($id);

        $input = $request->all();
        $post->fill($input);
        $post->user()->associate(Auth::user());

        $post->save();

        return redirect('admin/posts')->with('success', 'Post edited!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $post = Post::findOrFail($id);

        File::deleteDirectory(config('blog.path') . $post->id);

        $post->delete();
        return redirect('admin/posts')->with('success', 'Post deleted!');
    }

    public function activate($id) {
        $post = Post::findOrFail($id);
        $post->active = 1;
        $post->timestamps = false;
        $post->save();
        $post->timestamps = true;
        return redirect('admin/posts')->with('success', 'Post activated!');
    }
    public function deactivate($id) {
        $post = Post::findOrFail($id);
        $post->active = 0;
        $post->timestamps = false;
        $post->save();
        $post->timestamps = true;
        return redirect('admin/posts')->with('success', 'Post deactivated!');
    }
}
