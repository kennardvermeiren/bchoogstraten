<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Album;
use App\Image;

use File;
use Auth;

use App\Http\Requests\Album\CreateAlbumRequest;
use App\Http\Requests\Album\UpdateAlbumRequest;
use App\Http\Requests\Upload\UploadImageRequest;
use App\Http\Requests\Album\RemoveImageRequest;



class AlbumController extends Controller
{
    /**
    * the model instance
    * @var User
    */
    protected $album;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(Album $album)
    {
        $this->album = $album;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $albums = Album::orderBy('updated_at', 'desc')->paginate(15);
        
        return view('admin.albums.index', compact('albums'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.albums.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(CreateAlbumRequest $request)
    {
        $album = new Album;
        $album->fill($request->all());
        $album->user()->associate(Auth::user());
        
        $album->save();

        return redirect('admin/albums')->with('success', 'Album created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $album = Album::findOrFail($id);
        
        $images = $album->images;
        return view('admin.albums.show', compact('album', 'images'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $album = Album::findOrFail($id);
        return view('admin.albums.edit', compact('album'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, UpdateAlbumRequest $request)
    {
        $album = Album::findOrFail($id);

        $input = $request->all();
        $album->fill($input);
        $album->user()->associate(Auth::user());

        $album->save();

        return redirect('admin/albums')->with('success', 'Album edited!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $album = Album::findOrFail($id);
        $images = $album->images;

        File::deleteDirectory(config('gallery.path') . $album->id);

        foreach ($images as $image) {
            $image->delete();
        }
        $album->delete();
        return redirect('admin/albums')->with('success', 'Album deleted!');
    }

    public function getUpload($id) {
        $album = Album::findOrFail($id);
        return view('admin.albums.upload', compact('album'));
    }

    public function postUpload(UploadImageRequest $request) {
        $file = $request->file('file');
        $name = uniqid();
        $extension = $file->getClientOriginalExtension();

        $pathHiRes = config('gallery.path') . $request->input('id') . '/hires';
        $pathLowRes = config('gallery.path') . $request->input('id') . '/lowres';

        if (!file_exists($pathHiRes)) {
            File::makeDirectory($pathHiRes, $mode = 0777, true);
        }
        if (!file_exists($pathLowRes)) {
            File::makeDirectory($pathLowRes, $mode = 0777, true);
        }

        $img = \Img::make($file->getRealPath());
        $imgLowRes = \Img::make($file->getRealPath());
        $imgLowRes->resize(config('gallery.lowres_width'), null, function($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        $img->save($pathHiRes . '/' . $name . '.' . $extension);
        $imgLowRes->save($pathLowRes . '/' . $name . '.' . $extension);
        //$file->move($path, $name . '.' . $extension);
        //Storage::put($path . '/' . $name . '.' . $extension, file_get_contents($file));

        $entry = new Image();
        $entry->filename = $name;
        $entry->mime = $extension;
        $entry->album_id = $request->input('id');

        $entry->save();

        return redirect('admin.albums.index');
    }

    public function getRemove($id) {
        $album = Album::findOrFail($id);
        $images = $album->images;
        return view('admin.albums.remove', compact('album', 'images'));
    }

    public function postRemove($id, RemoveImageRequest $request) {
        $album = Album::findOrFail($id);
        foreach ($request->get('images') as $imgId) {
            $image = Image::findOrFail($imgId);
            $pathHiRes = config('gallery.path') . $id . '/hires/' . $image->filename . '.' . $image->mime;
            $pathLowRes = config('gallery.path') . $id . '/lowres/' . $image->filename . '.' . $image->mime;

            $image->delete();

            if (file_exists($pathHiRes) && file_exists($pathLowRes)) {
                File::delete($pathHiRes, $pathLowRes);
            }
        }
        return redirect('admin/albums')->with('success', 'Images deleted!');
    }

    public function activate($id) {
        $album = Album::findOrFail($id);
        $album->active = 1;
        $album->timestamps = false;
        $album->save();
        $album->timestamps = true;
        return redirect('admin/albums')->with('success', 'Album activated!');
    }
    public function deactivate($id) {
        $album = Album::findOrFail($id);
        $album->active = 0;
        $album->timestamps = false;
        $album->save();
        $album->timestamps = true;
        return redirect('admin/albums')->with('success', 'Album deactivated!');
    }
}
