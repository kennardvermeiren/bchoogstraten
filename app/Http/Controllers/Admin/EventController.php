<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Event;
use Auth;

use App\Http\Requests\Event\CreateEventRequest;
use App\Http\Requests\Event\UpdateEventRequest;


class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::orderBy('updated_at', 'desc')->paginate(15);
        
        return view('admin.events.index', compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.events.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateEventRequest $request)
    {
        $event = new Event;
        $event->fill($request->all());
        $event->user()->associate(Auth::user());
        
        $event->save();

        return redirect('admin/events')->with('success', 'Event created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event = Event::findOrFail($id);
        return view('admin.events.show', compact('event'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event = Event::findOrFail($id);
        return view('admin.events.edit', compact('event'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, UpdateEventRequest $request)
    {
        $event = Event::findOrFail($id);

        $input = $request->all();
        $event->fill($input);
        $event->user()->associate(Auth::user());

        $event->save();

        return redirect('admin/events')->with('success', 'Event edited!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = Event::findOrFail($id);

        $event->delete();
        return redirect('admin/events')->with('success', 'Event deleted!');
    }

    public function activate($id) {
        $event = Event::findOrFail($id);
        $event->active = 1;
        $event->timestamps = false;
        $event->save();
        $event->timestamps = true;
        return redirect('admin/events')->with('success', 'Event activated!');
    }
    public function deactivate($id) {
        $event = Event::findOrFail($id);
        $event->active = 0;
        $event->timestamps = false;
        $event->save();
        $event->timestamps = true;
        return redirect('admin/events')->with('success', 'Event deactivated!');
    }
}
