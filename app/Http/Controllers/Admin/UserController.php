<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Category;

use App\Http\Requests\User\CreateUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Http\Requests\User\UpdateActiveRequest;

use Illuminate\Contracts\Mail\Mailer as Mail;

use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;

class UserController extends Controller
{
    /**
    * the model instance
    * @var User
    */
    protected $user;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $users = User::all();
        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $categories = Category::orderBy('id')->lists('name', 'id');
        $countries = User::$COUNTRIES;
        return view('admin.users.create', compact('categories', $categories, 'countries', $countries));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(CreateUserRequest $request)
    {
        $this->user->email = $request->email;
        $this->user->first_name = $request->first_name;
        $this->user->last_name = $request->last_name;
        $this->user->birth_date = $request->birth_date;
        $this->user->category_id = $request->category;
        $this->user->country = $request->country;
        $this->user->city = $request->city;
        $this->user->postal_code = $request->postal_code;
        $this->user->address = $request->address;
        $this->user->bus = $request->bus;
        $this->user->mobile_phone = $request->mobile_phone;
        $this->user->telephone = $request->telephone;

        $this->user->password = bcrypt($request->password);
        $this->user->save();

        $user = $this->user;

        \Mail::send('emails.auth.account_creation', 
            ['first_name' => $user->first_name], 
            function ($message) use ($user) {
            $message->subject('BCHoogstraten account')
                ->to($user->email, $user->first_name.  ' ' . $user->last_name);
        });
        
        return redirect('admin/users')->with('success', 'User created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        return view('admin.users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $categories = Category::lists('name', 'id');
        $countries = User::$COUNTRIES;
        return view('admin.users.edit', compact('user', 'categories', 'countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, UpdateUserRequest $request)
    {
        $user = User::findOrFail($id);

        $input = $request->all();
        $user->fill($input);
        $user->category_id = $request->category;
        $user->save();
        return redirect('admin/users')->with('success', 'User edited!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return redirect('admin/users')->with('success', 'User deleted!');
    }

    public function updateActive(UpdateActiveRequest $request) {
        $user_ids = $request->input('confirmed');

        if($user_ids != null) {
            $users = User::where('admin', 0)->get();

            //activate users with matching id
            foreach ($users as $index => $user) {
                foreach ($user_ids as $id) {
                    if ($user->id == $id) {
                        //dd($user->id);
                        if ($user->confirmed != 1) {
                            $user->confirmed = 1;
                            if($user->save()) {
                                \Mail::queue('emails.auth.account_activation', array('first_name' => $user->first_name), function($message) use ($user) {
                                    $message->to($user->email, $user->first_name . ' ' . $user->last_name)->subject(trans('user.communication.activation.subject'));
                                });
                            }
                        }
                        unset($users[$index]);
                    }
                }
            }
            //deactivate remaining
            foreach ($users as $user) {
                if ($user->confirmed != 0) {
                    $user->confirmed = 0;
                    if($user->save()) {
                        \Mail::queue('emails.auth.account_deactivation', array('first_name' => $user->first_name), function($message) use ($user) {
                            $message->to($user->email, $user->first_name . ' ' . $user->last_name)->subject(trans('user.communication.deactivation.subject'));
                        });
                    }
                }
            }
        } else {
            $users = User::where('admin', 0)->get();
            //deactivate all
            foreach ($users as $user) {
                if ($user->confirmed != 0) {
                    $user->confirmed = 0;
                    if($user->save()) {
                        \Mail::queue('emails.auth.account_deactivation', array('first_name' => $user->first_name), function($message) use ($user) {
                            $message->to($user->email, $user->first_name . ' ' . $user->last_name)->subject(trans('user.communication.deactivation.subject'));
                        });
                    }
                }
            }
        }

        return redirect('admin/users')->with('success', 'Users updatedddd!');
    }

    public function deactivateAll() {
        $users = User::where('admin', 0)->get();
        foreach ($users as $user) {
            if($user->confirmed != 0) {
                $user->confirmed = 0;
                if($user->save()) {
                    \Mail::queue('emails.auth.account_deactivation', array('first_name' => $user->first_name), function($message) use ($user) {
                        $message->to($user->email, $user->first_name . ' ' . $user->last_name)->subject(trans('user.communication.deactivation.subject'));
                    });
                }
            }
        }

        return redirect('admin/users')->with('success', 'Users deactivated!');
    }

    public function exportTable() {
        $fields = [
            'first_name', 
            'last_name', 
            'email', 
            'birth_date', 
            'country', 
            'city', 
            'postal_code', 
            'address', 
            'bus', 
            'mobile_phone', 
            'telephone', 
            'category_id',
            'confirmed'
        ];
        $users = User::select($fields)->get();
        //dd($users);
        foreach ($users as $user) {
            if($user->country == 'be' | $user->country == 'nl') {
                $user->country = trans('user.countries.' . $user->country);
            }
            if($user->confirmed == 1) {
                $user->confirmed = trans('general.yes');
            } else {
                $user->confirmed = trans('general.no');
            }
            //$user->category_id = trans('category.' . Category::find($user->category_id)->select('name'));
        }

        \Excel::create(Carbon::now()->toDateTimeString(), function($excel) use ($users, $fields) {
            $excel->setTitle('Ledenlijst (' . Carbon::now()->toDateTimeString() . ')')
                    ->setCreator('BCHoogstraten website')
                    ->setCompany('BCHoogstraten');

            $excel->sheet('Ledenlijst', function($sheet) use ($users, $fields) {
                $sheet->fromModel($users);
                $fieldsTrans = array();
                foreach ($fields as $field) {
                    if ($field != 'category_id') {
                        $fieldsTrans[] = trans('user.' . $field);
                    } else {
                        $fieldsTrans[] = trans('category.' . $field);
                    }
                }
                $sheet->row(1, $fieldsTrans);
            });

        })->export('xls');
    }
}
