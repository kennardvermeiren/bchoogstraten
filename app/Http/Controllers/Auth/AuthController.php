<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use App\Category;
use Illuminate\Contracts\Auth\Guard;

use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;

use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use Illuminate\Contracts\Mail\Mailer as Mail;

use JavaScript;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    /**
     * the model instance
     * @var User
     */
    protected $user; 
    /**
     * The Guard implementation.
     *
     * @var Authenticator
     */
    protected $auth;


    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    protected $redirectPath = '/';
    protected $loginPath = '/auth/login';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(Guard $auth, User $user)
    {
        $this->user = $user; 
        $this->auth = $auth;

        $this->middleware('guest', ['except' => 'getLogout']);
    }


    /**
     * Show the application registration form.
     *
     * @return Response
     */
    public function getRegister()
    {
        //$categories = Category::lists('name', 'id');
        $categories = Category::all();
        $countries = User::$COUNTRIES;
        return view('auth.register', compact('categories', $categories, 'countries', $countries));
    }
 
    /**
     * Handle a registration request for the application.
     *
     * @param  RegisterRequest  $request
     * @return Response
     */
    public function postRegister(RegisterRequest $request)
    {
        //code for registering a user goes here.
        $this->user->email = $request->email;
        $this->user->first_name = $request->first_name;
        $this->user->last_name = $request->last_name;
        $this->user->birth_date = $request->birth_date;
        $this->user->category_id = $request->category;
        $this->user->country = $request->country;
        $this->user->city = $request->city;
        $this->user->postal_code = $request->postal_code;
        $this->user->address = $request->address;
        $this->user->bus = $request->bus;
        $this->user->mobile_phone = $request->mobile_phone;
        $this->user->telephone = $request->telephone;

        $this->user->password = bcrypt($request->password);
        $this->user->save();

        $user = $this->user;

        \Mail::send('emails.auth.account_creation', 
            ['first_name' => $user->first_name, 'categories' => Category::all()], 
            function ($message) use ($user) {
            $message->subject('BCHoogstraten account')
                ->to($user->email, $user->first_name.  ' ' . $user->last_name);
        });

        $request->session()->flash('success', trans('user.signup.message.success'));
        return redirect('auth/login');
    }
 
    /**
     * Show the application login form.
     *
     * @return Response
     */
    public function getLogin()
    {
        /*JavaScript::put([
            'success' => 'Het is gelukt! JAJAJA'
        ]);*/
        return view('auth.login');
    }
 
    /**
     * Handle a login request to the application.
     *
     * @param  LoginRequest  $request
     * @return Response
     */
    public function postLogin(LoginRequest $request)
    {
        if ($this->auth->attempt(['email' => $request->email, 'password' => $request->password], $request->remember))
        {
            //dd($this->auth->user()->confirmed);
            if ($this->auth->user()->confirmed != 1) {
                $this->auth->logout();
                return redirect('auth/login')->with('error', trans('auth.not_confirmed'));
            }
            //$request->session()->flash('success', trans('user.login.message.success'));
            JavaScript::put([
                'success' => trans('user.login.message.success')
            ]);
            return redirect('/')->with('success', trans('user.login.message.success'));
        }
        
        /*JavaScript::put([
            'error' => 'Het is NIET gelukt! JAJAJA'
        ]);*/
        return redirect('auth/login')->withErrors([
            'email' => trans('auth.failed'),
        ]);
    }
 
    /**
     * Log the user out of the application.
     *
     * @return Response
     */
    public function getLogout()
    {
        $this->auth->logout();
        //Auth::logout();
        return redirect('/');
    }
}
