<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Album;
use App\Image;
use Carbon\Carbon;


class AlbumController extends Controller
{
    public function index()
    {
        $albums = Album::where('active', 1)
            ->orderBy('updated_at', 'desc')
            ->paginate(config('gallery.albums_per_page'));

        return view('site.gallery', compact('albums'));
    }

    public function showAlbum($slug)
    {
        $album = Album::whereSlug($slug)->firstOrFail();
        $images = Image::where('album_id', '=', $album->id)->paginate(10);

        return view('site.album', compact('album', 'images'));
    }
}
