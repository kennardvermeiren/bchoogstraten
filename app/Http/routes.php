<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Carbon\Carbon::setLocale(config('app.locale'));

//Blog
Route::get('/', function () {
    return redirect('/blog');
});
Route::get('blog', 'BlogController@index');
Route::get('blog/{slug}', 'BlogController@showPost');

Route::get('calendar', 'EventController@index');
Route::get('calendar/{slug}', 'EventController@showEvent');

Route::get('gallery', 'AlbumController@index');
Route::get('gallery/{slug}', 'AlbumController@showAlbum');


Route::get('about', function () {
	$categories = App\Category::all();
    return view('site.about', compact('categories'));
});
Route::get('contact', 'ContactController@getContact');
Route::post('contact-sent', 'ContactController@sendMail');

Route::get('documents', function () {
    return view('site.documents');
});
Route::get('info', function () {
    return view('site.general-info');
});

Route::get('downloads/{filename}', function() {
    return Response::download(public_path() . 'site/docs/' . $filename);
});


Route::get('dashboard', 'DashBoardController@index');
Route::patch('dashboard/update', [
	'as' => 'dashboard.update',
	'uses' => 'DashBoardController@update'
]);

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');


Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function() {
	Route::get('/', function() {
		return redirect('admin/dashboard');
	});
	Route::get('dashboard', 'Admin\AdminController@index');

	Route::get('posts/{id}/activate', 'Admin\PostController@activate');
	Route::get('posts/{id}/deactivate', 'Admin\PostController@deactivate');
	Route::resource('posts', 'Admin\PostController');

	Route::get('events/{id}/activate', 'Admin\EventController@activate');
	Route::get('events/{id}/deactivate', 'Admin\EventController@deactivate');
	Route::resource('events', 'Admin\EventController');

	Route::post('users/update_active', 'Admin\UserController@updateActive');
	Route::get('users/deactivate_all', 'Admin\UserController@deactivateAll');
	Route::get('users/export_table', 'Admin\UserController@exportTable');
	Route::resource('users', 'Admin\UserController');

	Route::get('albums/{id}/add', 'Admin\AlbumController@getUpload');
	Route::post('albums/{id}/uploaded', 'Admin\AlbumController@postUpload');
	Route::get('albums/{id}/remove', 'Admin\AlbumController@getRemove');
	Route::post('albums/{id}/remove', 'Admin\AlbumController@postRemove');
	Route::get('albums/{id}/activate', 'Admin\AlbumController@activate');
	Route::get('albums/{id}/deactivate', 'Admin\AlbumController@deactivate');
	Route::resource('albums', 'Admin\AlbumController');
});