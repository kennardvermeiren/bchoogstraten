<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //dd(Auth::user()->admin);
        if(!Auth::check() || Auth::user()->admin == 0) {
            return redirect('/')->withErrors('You have no business here!');
        }

        return $next($request);
    }
}
