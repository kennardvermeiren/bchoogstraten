<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = 'images';

    protected $fillable = ['filename', 'mime'];

    public function album() {
    	return $this->belongsTo('Album');
    }
}
