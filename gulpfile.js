var elixir = require('laravel-elixir');

var bowerDir = './resources/assets/vendor';
 
var paths = {
    'bootstrap' : bowerDir + "/bootstrap-sass-official/assets",
    'fontawesome' : bowerDir + "/font-awesome",
    'jquery': bowerDir + '/jquery',
    'dropzone': bowerDir + '/dropzone/dist',
    'lightbox' : bowerDir + '/lightbox2/dist',
    'infinitescroll' : bowerDir + '/jquery-infinite-scroll',
    'isotope' : bowerDir + '/isotope/dist',
    'imagepicker' : bowerDir + '/image-picker/image-picker',
    'summernote' : bowerDir + '/summernote/dist',
    'datepicker' : bowerDir + '/bootstrap-datepicker/dist',
    'notie' : bowerDir + '/notie'
};
 
elixir(function(mix) {
    mix.sass('app.scss', '/public/assets/css', { includePaths: [paths.bootstrap + '/stylesheets', paths.fontawesome + '/scss'] })
    .scripts([
        paths.jquery + '/dist/jquery.min.js',
        paths.bootstrap + '/javascripts/bootstrap.min.js',
        paths.notie + '/notie.js'
        ], '/public/assets/js/vendor.js', bowerDir)
    .copy('resources/assets/js/app.js', '/public/assets/js/app.js')
    //DROPZONE
    .copy(paths.dropzone + '/min/dropzone.min.js', '/public/assets/js/dropzone.js')
    .copy(paths.dropzone + '/min/dropzone.min.css', '/public/assets/css/dropzone.css')
    //LIGHTBOX
    .copy(paths.lightbox + '/js/lightbox.min.js', '/public/assets/js/lightbox.js')
    .copy(paths.lightbox + '/css/lightbox.css', '/public/assets/css/lightbox.css')
    .copy(paths.lightbox + '/images/**', '/public/assets/images')
    //JSCROLL
    .copy(paths.infinitescroll + '/jquery.infinitescroll.min.js', '/public/assets/js/infinitescroll.min.js')
    //ISOTOPE
    .copy(paths.isotope + '/isotope.pkgd.min.js', '/public/assets/js/isotope.min.js')
    //IMAGE PICKER
    .copy(paths.imagepicker + '/image-picker.css', '/public/assets/css/image-picker.css')
    .copy(paths.imagepicker + '/image-picker.min.js', '/public/assets/js/image-picker.min.js')
    //SUMMERNOTE
    .copy(paths.summernote + '/summernote.css', '/public/assets/css/summernote.css')
    .copy(paths.summernote + '/summernote.min.js', '/public/assets/js/summernote.js')
    //DATEPICKER
    .copy(paths.datepicker + '/css/bootstrap-datepicker3.min.css', '/public/assets/css/bootstrap-datepicker3.min.css')
    .copy(paths.datepicker + '/js/bootstrap-datepicker.min.js', '/public/assets/js/bootstrap-datepicker.min.js')
    //FONTS
    .copy(paths.bootstrap + '/fonts/bootstrap/**', '/public/assets/fonts')
    .copy(paths.fontawesome + '/fonts', '/public/assets/fonts');
});
