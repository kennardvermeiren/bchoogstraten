<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique();

            $table->date('birth_date');
            $table->enum('country', App\User::$COUNTRIES);
            $table->string('city');
            $table->string('postal_code', 10);
            $table->string('address');
            $table->string('bus')->nullable();
            $table->string('mobile_phone', 20)->nullable();
            $table->string('telephone', 20)->nullable();

            $table->unsignedInteger('category_id');

            $table->string('password', 60);
            $table->string('confirmation_code');
            $table->boolean('confirmed')->default(false);
            $table->boolean('admin')->default(false);
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('category_id')->references('id')->on('member_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }

}
