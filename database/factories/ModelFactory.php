<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use Faker\Factory as Faker;

$factory->define(App\User::class, function ($faker) {
    $faker = Faker::create('nl_NL');
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->unique()->email,
        'category_id' => $faker->numberBetween(1, 5),
        'country' => 'be',
        'city' => $faker->city,
        'postal_code' => $faker->randomNumber(4),
        'address' => $faker->streetName . ' ' . $faker->numberBetween(0, 1000),
        'birth_date' => $faker->date(),
        'telephone' => $faker->randomNumber(9),
        'mobile_phone' => $faker->randomNumber(9),
        'password' => bcrypt(str_random(10)),
        'confirmed' => $faker->boolean(50),
        'confirmation_code' => md5(microtime() . env('APP_KEY')),
        'created_at' => $faker->dateTime(),
    ];
});

$factory->define(App\Post::class, function ($faker) {
  $faker = Faker::create('nl_NL');
  return [
    'title' => $faker->sentence(mt_rand(3, 10)),
    'summary' => $faker->paragraph(mt_rand(4, 10)),
    'body' => join("\n\n", $faker->paragraphs(mt_rand(3, 6))),
    'active' => $faker->boolean(50),
    'user_id' => (int) rand(1,2),
    'created_at' => $faker->dateTime(),
  ];
});

$factory->define(App\Album::class, function ($faker) {
  $faker = Faker::create('nl_NL');
  return [
    'title' => $faker->sentence(mt_rand(3, 10)),
    'summary' => $faker->paragraph(mt_rand(4, 10)),
    'body' => join("\n\n", $faker->paragraphs(mt_rand(3, 6))),
    'active' => $faker->boolean(50),
    'user_id' => (int) rand(1,2),
    'created_at' => $faker->dateTime(),
  ];
});

$factory->define(App\Event::class, function ($faker) {
  $faker = Faker::create('nl_NL');
  return [
    'title' => $faker->sentence(mt_rand(3, 10)),
    'body' => join("\n\n", $faker->paragraphs(mt_rand(3, 6))),
    //'begin' => date('2016-3-10 14:00:00'),
    //'end' => date('2016-3-10 18:00:00'),
    'location' => $faker->address,
    'active' => $faker->boolean(50),
    'user_id' => (int) rand(1,2),
    'created_at' => $faker->dateTime(),
  ];
});
