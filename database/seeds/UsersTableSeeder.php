<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder {

	public function run()
	{

		\App\User::create([
			'first_name' => 'Admin',
			'last_name' => 'bc',
			'email' => 'admin@admin.com',
			'birth_date' => '1990-01-01',
			'country' => 'be',
			'city' => 'Antwerpen',
			'postal_code' => '2000',
			'address' => 'Meir 1',
			'category_id' => 1,

			'password' => bcrypt('admin'),
			'confirmed' => 1,
            'admin' => 1,
			'confirmation_code' => md5(microtime() . env('APP_KEY')),
		]);

		\App\User::create([
			'first_name' => 'User',
			'last_name' => 'bc',
			'email' => 'user@user.com',
			'birth_date' => '1990-01-01',
			'country' => 'be',
			'city' => 'Antwerpen',
			'postal_code' => '2000',
			'address' => 'Meir 1',
			'category_id' => 1,
			
			'password' => bcrypt('user'),
			'confirmed' => 1,
			'confirmation_code' => md5(microtime() . env('APP_KEY')),
		]);

    	factory(App\User::class, 20)->create();

	}

}
