<?php

use Illuminate\Database\Seeder;

class MemberCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('member_categories')->insert([
            'name' => 'adult_comp',
            'price' => 35.00,
        ]);

        DB::table('member_categories')->insert([
            'name' => 'adult_rec',
            'price' => 30.00,
        ]);

        DB::table('member_categories')->insert([
            'name' => 'youth',
            'price' => 30.00,
        ]);

        DB::table('member_categories')->insert([
            'name' => 'minibad',
            'price' => 25.00,
        ]);

        DB::table('member_categories')->insert([
            'name' => 'gbad',
            'price' => 25.00,
        ]);
    }
}
