<?php

return [
    'title' => 'Titel',
    'summary' => 'Inleiding (OPTIONEEL)',
    'body' => 'Inhoud',
    'active' => 'Zichtbaar',
    'created_at' => 'Gemaakt op ',
    'updated_at' => 'Geüpdate op ',

    'no-summary' => 'Eerste regels van de inhoud als inleiding'
];
