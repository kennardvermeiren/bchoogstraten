<?php

return [
    'title' => 'Titel',
    'summary' => 'Inleiding',
    'body' => 'Inhoud',
    'active' => 'Zichtbaar',
    'created_at' => 'Gemaakt op ',
    'updated_at' => 'Geüpdate op ',

    'no-summary' => 'Eerste regels van de inhoud als inleiding',
    
    'image_amount' => 'Aantal foto\'s'
];
