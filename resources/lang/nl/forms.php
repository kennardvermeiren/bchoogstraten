<?php

return [
    'field' => [
        'message' => 'Bericht',
        'email' => 'Emailadres',
        'first_name' => 'Volwassene recreatief',
        'last_name' => 'Jeugd',
        'password' => 'Minibad',
        'password_confirmation' => 'G-Bad',
        'birth_date' => 'Bericht',
        'category' => 'Bericht',
        'country' => 'Bericht',
        'city' => 'Bericht',
        'postal_code' => 'Bericht',
        'address' => 'Bericht',
        'bus' => 'Bericht',
        'mobile_phone' => 'Bericht',
        'telephone' => 'Bericht'
    ],
    'button' => [
        'submit' => 'Verzenden',
        'cancel' => 'Annuleren'
    ]
];
