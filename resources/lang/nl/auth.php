<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'We hebben deze gegevens niet teruggevonden in de databank. Probeer het nog eens?',
    'throttle' => 'Te veel pogingen. Probeer het opnieuw binnen :seconds seconden.',
    'not_confirmed' => 'Uw gebruiker is nog niet geactiveerd!',
];
