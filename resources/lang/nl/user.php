<?php

return [
    'email' => 'Emailadres',
    'first_name' => 'Voornaam',
    'last_name' => 'Achternaam',
    'password' => 'Wachtwoord',
    'password_confirmation' => 'Wachtwoord herhaling',
    'birth_date' => 'Geboortedatum',
    'category' => 'Categorie',
    'country' => 'Land',
    'countries' => [
        'be' => 'België',
        'nl' => 'Nederland'
    ],
    'city' => 'Stad',
    'postal_code' => 'Post code',
    'address' => 'Adres',
    'bus' => 'Bus',
    'mobile_phone' => 'GSM nr.',
    'telephone' => 'Telefoon nr.',

    'confirmed' => 'Betaald',

    'guest' => 'Gast',

    'signup' => [
        'title' => 'Inschrijven',
        'btnsubmit' => 'Inschrijven',
        'btncancel' => 'Annuleren',
        'message' => [
            'success' => 'Bedankt voor uw registratie. Binnenkort krijgt u een email van ons met meer informatie.',
            'error' => 'Er is iets misgelopen. Probeer het later opnieuw.'
        ]
    ],
    'login' => [
        'title' => 'Aanmelden',
        'btnsubmit' => 'Aanmelden',
        'btnforgot' => 'Wachtwoord vergeten',
        'message' => [
            'success' => 'U bent succesvol aangemeld!',
            'error' => ''
        ]
    ],
    'settings' => [
        'title' => 'Uw informatie',
        'btnsubmit' => 'Aanpassen',
        'btncancel' => 'Annuleren',
        'message' => [
            'success' => 'Uw gegevens zijn succesvol aangepast!',
            'error' => 'Er is iets misgelopen..'
        ]
    ],
    'password_reset' => [
        'title' => 'Wachtwoord herstel',
        'btnsubmit' => 'Versturen',
        'btncancel' => 'Annuleren',
        'message' => [
            'success' => 'Controleer uw email voor verdere instructies.',
            'error' => 'Er is iets misgelopen.. Probeer het later nog eens.'
        ]
    ],
    'reset' => [
        'title' => 'Wachtwoord herstel',
        'btnsubmit' => 'Herstellen',
        'btncancel' => 'Annuleren',
        'message' => [
            'success' => 'Uw wachtwoord is succesvol hersteld!',
            'error' => 'Er is iets misgelopen.. Probeer het later nog eens.'
        ]
    ],

    'communication' => [
        'creation' => [
            'subject' => 'Account registratie  - BCHoogstraten',
            'greetings' => 'Beste ',
            'body' => 'Uw BCHoogstraten account is succesvol aangemaakt. Uw account wordt geactiveerd nadat we uw betaling op rekeningnummer BE64 8508 7669 1452 hebben ontvangen.<br>
            Hieronder nog eens het bedrag per categorie:',
            'farewell' => 'Met vriendelijke groeten<br><br>Badmintonclub Hoogstraten VZW'
        ],

        'activation' => [
            'subject' => 'Account activatie - BCHoogstraten',
            'greetings' => 'Beste ',
            'body' => 'Uw betaling is bevestigd. Hierbij laten wij u weten dat u vanaf nu kan aanmelden via de website met de gegevens die u heeft ingevoerd tijdens het registreren.',
            'farewell' => 'Met vriendelijke groeten<br><br>
            Badmintonclub Hoogstraten VZW'
        ],

        'deactivation' => [
            'subject' => 'Account deactivatie - BCHoogstraten',
            'greetings' => 'Beste sportieveling',
            'body' => 'Nu het seizoen ten einde is worden alle accounts gedeactiveerd. Wanneer wij uw betaling voor volgend seizoen ontvangen wordt uw account opnieuw geactiveerd.',
            'farewell' => 'Sportieve groeten<br><br>
            Badmintonclub Hoogstraten VZW'
        ],

        'password' => [
            'subject' => 'Wachtwoord herstellen - BCHoogstraten',
            'greetings' => 'Beste ',
            'body' => 'Gebruik onderstaande link om uw wachtwoord te veranderen.',
            'farewell' => 'Met vriendelijke groeten<br><br>
            Badmintonclub Hoogstraten VZW'
        ]
    ]
];
