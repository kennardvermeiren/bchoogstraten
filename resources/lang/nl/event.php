<?php

return [
    'title' => 'Titel',
    'body' => 'Inhoud',
    'begin' => 'Van',
    'end' => 'Tot',
    'location' => 'Locatie',
    'active' => 'Zichtbaar',
    'created_at' => 'Gemaakt op ',
    'updated_at' => 'Geüpdate op ',
];
