<?php

return [

    'adult_comp' => 'Volwassene competitie',
    'adult_rec' => 'Volwassene recreatief',
    'youth' => 'Jeugd',
    'minibad' => 'Minibad',
    'gbad' => 'G-Bad'
];
