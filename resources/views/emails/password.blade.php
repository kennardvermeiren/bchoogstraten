<h1>{{ trans('user.communication.password.subject') }}</h1>

<p>{{ trans('user.communication.password.greetings') }}</p>

<p>{{ trans('user.communication.password.body') }}</p>
<a href="{!! url('password/reset/'.$token) !!}">Klik hier</a>

<p>{{ trans('user.communication.password.farewell') }}</p>