<h1>{!! Lang::get('user.communication.creation.subject') !!}</h1>

<p>{!! Lang::get('user.communication.creation.greetings') !!} {{ $first_name }}</p>

<p>{!! Lang::get('user.communication.creation.body') !!}</p>
<ul>
@foreach ($categories as $category)
	<li>{!! Lang::get('category.' . $category->name) !!} - €{{$category->price}}
@endforeach
</ul>



<p>{!! Lang::get('user.communication.creation.farewell') !!}</p>