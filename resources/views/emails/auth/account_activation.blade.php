<h1>{!! Lang::get('user.communication.activation.subject') !!}</h1>

<p>{!! Lang::get('user.communication.activation.greetings') !!} {{ $first_name }}</p>

<p>{!! Lang::get('user.communication.activation.body') !!}</p>


<p>{!! Lang::get('user.communication.activation.farewell') !!}</p>