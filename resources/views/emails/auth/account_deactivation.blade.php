<h1>{!! Lang::get('user.communication.deactivation.subject') !!}</h1>

<p>{!! Lang::get('user.communication.deactivation.greetings') !!} {!! $first_name !!}</p>

<p>{!! Lang::get('user.communication.deactivation.body') !!}</p>


<p>{!! Lang::get('user.communication.deactivation.farewell') !!}</p>