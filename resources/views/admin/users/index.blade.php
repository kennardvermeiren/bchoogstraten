@extends('layout.master')

@section('js')
	<script type="text/javascript">
		$("table").on("change", ":checkbox", function() {
			if($(this).parents("tr:first").hasClass('highlight')) {
				$(this).parents("tr:first").removeClass('highlight');
			} else {
				$(this).parents("tr:first").addClass('highlight');
				$('#active-changed-alert').removeClass('hidden');
			}
		});
	</script>
@endsection

{{-- Content --}}
@section('content')
@include('admin.users.partials.navigation')

<!-- div -->
<div class="row">
	<div class="col-xs-12">
		<ul class="list-inline">
			<li>
			{!! Form::open(array('url' => 'admin/users/update_active', 'id' => 'form-confirmation')) !!}
				<div id="submit-confirmed" class="form-actions form-group">
					<button onclick="return confirm('Je gaat de status van alle gebruikers veranderen zoals ze NU ingesteld staan. Elke aangepaste gebruiker zal een email ontvangen. Ben je zeker?')" form="form-confirmation" type="submit" class="btn btn-primary btn-lg" data-toggle="tooltip" data-placement="top" title="Update de betaald-status van alle gebruikers zoals ze nu ingesteld zijn">
						<span class="fa-stack"><i class="fa fa-toggle-on fa-stack-1x"></i></span> 
						<span class="hidden-xs">Update betaald status van alle leden</span>
					</button>
		        </div>
			{!! Form::close() !!}
		    </li>
		    <li>
		    <a class="btn btn-warning btn-lg btn-loading" href="{{ URL::to('admin/users/deactivate_all') }}" data-loading-text="<span class='fa-stack'><i class='fa fa-refresh fa-spin fa-stack-1x'></i></span> Bezig met verwerken.." data-toggle="tooltip" data-placement="top" title="Deactiveer iedereen behalve administrators">
			    <span class="fa-stack"><i class="fa fa-users fa-stack-1x"></i><i class="fa fa-ban fa-stack-2x text-danger"></i></span>
			    <span class="hidden-xs">Deactiveer alle leden</span>
		    </a>
		    </li>
		    <li>
			    <a class="btn btn-success btn-lg btn-loading" href="{{ URL::to('admin/users/export_table') }}" data-loading-text="<span class='fa-stack'><i class='fa fa-refresh fa-spin fa-stack-1x'></i></span> Bezig met verwerken.." data-toggle="tooltip" data-placement="top" title="Exporteer de tabel naar excel">
				    <span class="fa-stack"><i class="fa fa-file-excel-o fa-stack-1x"></i></span>
				    <span class="hidden-xs">Exporteer naar Excel</span>
			    </a>
		    </li>
	    </ul>

	    <div class="alert alert-warning alert-dismissible hidden" role="alert" id="active-changed-alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			De betaald-status van één of meerdere gebruikers is aangepast!
		</div>


		<table id="users" class="table">
			<thead>
				<tr>
					<th class="col-xs-2">{{{ Lang::get('user.first_name') }}}</th>
					<th class="col-xs-2">{{{ Lang::get('user.last_name') }}}</th>
					<th class="col-xs-3">{{{ Lang::get('user.email') }}}</th>
					<th class="col-xs-1">{{{ Lang::get('user.confirmed') }}}</th>
					<th class="col-xs-2">{{{ Lang::get('user.category') }}}</th>
					<th class="col-xs-2">{{{ Lang::get('admin.table_actions') }}}</th>
				</tr>
			</thead>
			<tbody>
				@foreach($users as $user)
					<tr>
						<td>{{ $user->first_name }}</td>
						<td>{{ $user->last_name }}</td>
						<td>{{ $user->email }}</td>
						<td>
						@if (!$user->admin)
							{!! Form::checkbox('confirmed[]', $user->id, $user->confirmed, array('form' => 'form-confirmation', 'class' => 'ckb-confirmed')) !!}
						@else
							<input type="checkbox" checked disabled>
						@endif
						{!! ($user->confirmed) ? Lang::get('general.yes') : Lang::get('general.no') !!}
						</td>
						<td>{{ Lang::get('category.' . $user->category->name) }}</td>
						<td>
							<div class="btn-group">
								<a class="btn btn-sm btn-info" href="{{ URL::to('admin/users/' . $user->id) }}" data-toggle="tooltip" data-placement="top" title="Toon gegevens"><i class="fa fa-info"></i></a>
								<a class="btn btn-sm btn-primary" href="{{ URL::to('admin/users/' . $user->id . '/edit') }}" data-toggle="tooltip" data-placement="top" title="Wijzig gegevens"><i class="fa fa-pencil"></i></a>
							</div>
							<div class="pull-right">
								<!-- delete the nerd (uses the destroy method DESTROY /nerds/{id} -->
								<!-- we will add this later since its a little more complicated than the other two buttons -->
								@if (!$user->admin)
									{!! Form::open([
							            'method' => 'DELETE',
							            'route' => ['admin.users.destroy', $user->id]]) 
							        !!}
							            {!! Form::button('<i class="fa fa-trash-o"></i>', 
							            	[	'type' => 'submit',
							            		'class' => 'btn btn-danger',
							            		'onclick' => 'return confirm("Verwijderen: Ben je zeker?")'
							            	]) 
							            !!}
							        {!! Form::close() !!}
								@endif
							</div>
						</td>
					</tr>
					@endforeach
			</tbody>
		</table>
	</div>
</div>
<!-- ./ div -->

@stop
