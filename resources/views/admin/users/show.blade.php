@extends('layout.master')

{{-- Content --}}
@section('content')
@include('admin.users.partials.navigation')

<!-- div -->
<div class="row">
	<div class="col-xs-12">
		<div class="jumbotron">
		<h2>{{{ $user->first_name . ' ' . $user->last_name }}}</h2>
		<p><strong>{{{ trans('user.birth_date') }}}:</strong> {{{ $user->birth_date }}}</p>
		<p><strong>{{{ trans('user.email') }}}:</strong> {{{ $user->email }}}</p>
		<p><strong>{{{ trans('user.category') }}}:</strong> {{{ trans('category.' . $user->category->name) }}}</p>
		<p><strong>{{{ trans('user.country') }}}:</strong> {{{ trans('user.countries.' . $user->country) }}}</p>
		<p><strong>{{{ trans('user.city') }}}:</strong> {{{ $user->city }}}</p>
		<p><strong>{{{ trans('user.postal_code') }}}:</strong> {{{ $user->postal_code }}}</p>
		<p><strong>{{{ trans('user.address') }}}:</strong> {{{ $user->address }}}</p>
		<p><strong>{{{ trans('user.bus') }}}:</strong> {{{ $user->bus }}}</p>
		<p><strong>{{{ trans('user.mobile_phone') }}}:</strong> {{{ $user->mobile_phone }}}</p>
		<p><strong>{{{ trans('user.telephone') }}}:</strong> {{{ $user->telephone }}}</p>
	</div>
	</div>
</div>
<!-- ./ div -->

@stop
