@extends('layout.master')

@section('css')
    {!! Html::style('assets/css/bootstrap-datepicker3.min.css') !!}
@endsection
@section('js')
    {!! Html::script('assets/js/bootstrap-datepicker.min.js') !!}
    <script>
        if (!Modernizr.inputtypes.date) { 
            $( "input[type=date]" ).datepicker();
        } 
    </script>
@endsection

{{-- Content --}}
@section('content')
@include('admin.users.partials.navigation')

<!-- div -->
<div class="row">
	<div class="col-xs-12">
		{!! Form::model($user, ['method' => 'PATCH', 'route' => ['admin.users.update', $user->id]]) !!}
	
		<div class="row">
	        <div class="form-group col-xs-12 col-sm-6 @if ($errors->has('first_name')) has-error @endif">
	            {!! Form::label('first_name', trans('user.first_name')) !!}
	            {!! Form::text('first_name', null, array('class' => 'form-control')) !!}
	            @if ($errors->has('first_name')) <span class="help-block">{!! $errors->first('first_name') !!}</span> @endif
	        </div>

	        <div class="form-group col-xs-12 col-sm-6 @if ($errors->has('last_name')) has-error @endif">
	            {!! Form::label('last_name', trans('user.last_name')) !!}
	            {!! Form::text('last_name', null, array('class' => 'form-control')) !!}
	            @if ($errors->has('last_name')) <span class="help-block">{!! $errors->first('last_name') !!}</span> @endif
	        </div>
	    </div>

	    <div class="row">
	        <div class="form-group col-xs-12 col-sm-6 @if ($errors->has('email')) has-error @endif">
	            {!! Form::label('email', trans('user.email')) !!}
	            {!! Form::email('email', null, array('class' => 'form-control')) !!}
	            @if ($errors->has('email')) <span class="help-block">{!! $errors->first('email') !!}</span> @endif
	        </div>
	    </div>

	    <div class="row">
	        <div class="form-group col-xs-12 col-sm-4 @if ($errors->has('birth_date')) has-error @endif">
	            {!! Form::label('birth_date', trans('user.birth_date')) !!}
	            {!! Form::input('date', 'birth_date', null ,array('class' => 'form-control datepicker', 'data-date-format' => 'yyyy-mm-dd')) !!}
	            @if ($errors->has('birth_date')) <span class="help-block">{!! $errors->first('birth_date') !!}</span> @endif
	        </div>

	        <div class="form-group col-xs-12 col-sm-4 @if ($errors->has('category')) has-error @endif">
	            {!! Form::label('category', trans('user.category')) !!}
	            <select id="category" name="category" class="form-control">
					@foreach($categories as $id => $name)
						<option value="{!! $id !!}" @if($id == $user->category_id) selected @endif>{!!trans('category.' . $name)!!}</option>
					@endforeach
				</select>
	            @if ($errors->has('category')) <span class="help-block">{!! $errors->first('category') !!}</span> @endif
	        </div>       
	    </div>

	    <div class="row">
	        <div class="form-group col-xs-12 col-sm-4 @if ($errors->has('category')) has-error @endif">
	            {!! Form::label('country', trans('user.country')) !!}
	            <select id="country" name="country" class="form-control">	
					@foreach($countries as $id => $name)
						<option value="{!! $name !!}" @if($name == $user->country) selected @endif>{!!trans('user.countries.' . $name)!!}</option>
					@endforeach
				</select>
	            @if ($errors->has('category')) <span class="help-block">{!! $errors->first('category') !!}</span> @endif
	        </div>

	        <div class="form-group col-xs-12 col-sm-4 @if ($errors->has('city')) has-error @endif">
	            {!! Form::label('city', trans('user.city')) !!}
	            {!! Form::text('city', null, array('class' => 'form-control')) !!}
	            @if ($errors->has('city')) <span class="help-block">{!! $errors->first('city') !!}</span> @endif
	        </div>

	        <div class="form-group col-xs-12 col-sm-4 @if ($errors->has('postal_code')) has-error @endif">
	            {!! Form::label('postal_code', trans('user.postal_code')) !!}
	            {!! Form::text('postal_code', null, array('class' => 'form-control')) !!}
	            @if ($errors->has('postal_code')) <span class="help-block">{!! $errors->first('postal_code') !!}</span> @endif
	        </div>
	    </div>

	    <div class="row">
	        <div class="form-group col-xs-12 col-sm-6 @if ($errors->has('address')) has-error @endif">
	            {!! Form::label('address', trans('user.address')) !!}
	            {!! Form::text('address', null, array('class' => 'form-control')) !!}
	            @if ($errors->has('address')) <span class="help-block">{!! $errors->first('address') !!}</span> @endif
	        </div>

	        <div class="form-group col-xs-12 col-sm-2 @if ($errors->has('bus')) has-error @endif">
	            {!! Form::label('bus', trans('user.bus')) !!}
	            {!! Form::text('bus', null, array('class' => 'form-control')) !!}
	            @if ($errors->has('bus')) <span class="help-block">{!! $errors->first('bus') !!}</span> @endif
	        </div>
	    </div>

	    <div class="row">
	        <div class="form-group col-xs-12 col-sm-6 @if ($errors->has('mobile_phone')) has-error @endif">
	            {!! Form::label('mobile_phone', trans('user.mobile_phone')) !!}
	            {!! Form::text('mobile_phone', null, array('class' => 'form-control')) !!}
	            @if ($errors->has('mobile_phone')) <span class="help-block">{!! $errors->first('mobile_phone') !!}</span> @endif
	        </div>

	        <div class="form-group col-xs-12 col-sm-6 @if ($errors->has('telephone')) has-error @endif">
	            {!! Form::label('telephone', trans('user.telephone')) !!}
	            {!! Form::text('telephone', null, array('class' => 'form-control')) !!}
	            @if ($errors->has('telephone')) <span class="help-block">{!! $errors->first('telephone') !!}</span> @endif
	        </div>
	    </div>

		<div class="form-group form-actions">
			{!! Form::submit('Submit', array('class' => 'btn btn-primary')) !!}
		</div>

	{!! Form::close() !!}
	</div>
</div>
<!-- ./ div -->

@stop
