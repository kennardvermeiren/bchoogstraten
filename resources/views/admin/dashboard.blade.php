@extends('layout.master')

{{-- Content --}}
@section('content')

<!-- div -->
<div class="row">
	<div class="col-xs-12">
		<h1>Welkom bij het administrator paneel!</h1>
		<p class="lead">Navigeer naar de verschillende modules met de knoppen die je linkboven ziet. Keer terug naar het publieke gedeelte met de knop aan de rechterkant!</p>
	</div>
</div>
<!-- ./ div -->

@stop
