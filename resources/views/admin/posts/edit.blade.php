@extends('layout.master')

@section('css')
	{!! Html::style('assets/css/summernote.css') !!}
@endsection

@section('js')
	{!! Html::script('assets/js/summernote.js') !!}
	<script type="text/javascript">
		$('#summary').summernote({
				height: 100,
				toolbar: [
				    ['style', ['bold', 'italic', 'underline', 'clear']],
				    ['font', ['strikethrough', 'superscript', 'subscript']],
				    ['fontsize', ['fontsize']],
				    ['color', ['color']],
				    ['para', ['ul', 'ol', 'paragraph']],
				    ['insert', ['link', 'video', 'table', 'hr']]
				]
			});
			$('#body').summernote({
				height: 300,
				toolbar: [
				    ['style', ['bold', 'italic', 'underline', 'clear']],
				    ['font', ['strikethrough', 'superscript', 'subscript']],
				    ['fontsize', ['fontsize']],
				    ['color', ['color']],
				    ['para', ['ul', 'ol', 'paragraph']],
				    ['insert', ['link', 'video', 'table', 'hr']]
				]
			});
	</script>
@endsection

{{-- Content --}}
@section('content')
@include('admin.posts.partials.navigation')

<!-- div -->
<div class="row">
	<div class="col-xs-12">
		{!! Form::model($post, ['method' => 'PATCH', 'route' => ['admin.posts.update', $post->id]]) !!}
		<div class="row">
			<div class="form-group @if ($errors->has('title')) has-error @endif">
				<div class="col-xs-10">
					{!! Form::label('title', trans('post.title')) !!}
					{!! Form::text('title', null, array('class' => 'form-control')) !!}
					@if ($errors->has('title')) <span class="help-block">{!! $errors->first('title') !!}</span> @endif
				</div>
			</div>

			<div class="form-group @if ($errors->has('active')) has-error @endif">
				<div class="col-xs-2">
					{!! Form::label('active', trans('post.active')) !!}
					<input type="checkbox" name="active" value="1" id="active" class="form-control" {!! $post->active ? "checked" : "" !!}>
					@if ($errors->has('active')) <span class="help-block">{!! $errors->first('active') !!}</span> @endif
				</div>
			</div>
		</div>

		<div class="row">
			<div class="form-group @if ($errors->has('summary')) has-error @endif">
				<div class="col-xs-12">
					{!! Form::label('summary', trans('post.summary')) !!}
					{!! Form::textarea('summary', Input::old('summary'), array('class' => 'summernote form-control', 'id' => 'summary')) !!}
					@if ($errors->has('summary')) <span class="help-block">{!! $errors->first('summary') !!}</span> @endif
				</div>
			</div>
		</div>

		<div class="row">
			<div class="form-group @if ($errors->has('body')) has-error @endif">
				<div class="col-xs-12">
					{!! Form::label('body', trans('post.body')) !!}
					{!! Form::textarea('body', null, array('class' => 'summernote form-control', 'id' => 'body')) !!}
					@if ($errors->has('body')) <span class="help-block">{!! $errors->first('body') !!}</span> @endif
				</div>
			</div>
		</div>

		<div class="form-group form-actions">
			{!! Form::submit('Submit', array('class' => 'btn btn-primary')) !!}
		</div>

	{!! Form::close() !!}
	</div>
</div>
<!-- ./ div -->

@stop
