@extends('layout.master')

{{-- Content --}}
@section('content')
@include('admin.posts.partials.navigation')

<!-- div -->
<div class="row">
	<div class="col-xs-12">
		<a class="btn btn-default btn-lg btn-block" href="{{ URL::to('admin/posts/' . $post->id . '/edit') }}" data-toggle="tooltip" data-placement="top" title="Wijzig gegevens"><span class="glyphicon glyphicon-pencil"></span> Wijzigen</a>
		<h1><strong>{{{ trans('post.title') }}}:</strong> {!! $post->title !!}</h1>
		<h2><strong>{{{ trans('post.summary') }}}:</strong></h2>
		<p>
			{!! $post->summary !!}
		</p>
		<h2><strong>{{{ trans('post.body') }}}:</strong></h2>
		<p>
			{!! $post->body !!}
		</p>
	</div>
</div>
<!-- ./ div -->

@stop
