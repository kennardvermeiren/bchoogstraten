<div class="page-header">
	<div class="pull-right">
		<a class="btn btn-default" href="{{ URL::to('admin/posts') }}"><i class="fa fa-list"></i><span class="hidden-xs"> Overzicht</span></a>
		<a class="btn btn-default" href="{{ URL::to('admin/posts/create') }}"><i class="fa fa-plus-square"></i></span><span class="hidden-xs"> Nieuw</span></a>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="alert alert-warning alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong class="lead">Aandacht: Tekst kopiëren</strong><br>
			Gelieve na het plakken van tekst, de geplakte tekst te selecteren en op op de knop "Remove font style (CTRL +\)" te klikken.
		</div>
	</div>
</div>