@extends('layout.master')

{{-- Content --}}
@section('content')
@include('admin.posts.partials.navigation')

<!-- div -->
<div class="row">
	<div class="col-xs-12">
		<table id="posts" class="table table-striped table-hover">
			<thead>
				<tr>
					<th class="col-xs-3">{{{ trans('post.title') }}}</th>
					<th class="col-xs-6">{{{ trans('post.summary') }}}</th>
					<th class="col-xs-1">{{{ trans('post.updated_at') }}}</th>
					<th class="col-xs-2">{{{ trans('admin.table_actions') }}}</th>
				</tr>
			</thead>
			<tbody>
				@foreach($posts as $post)
					<tr>
						<td><a href="{{ URL::to('admin/posts/' . $post->id) }}">{{ $post->title }}</a></td>
						<td>{{ str_limit(strip_tags($post->summary, 150)) }}</td>
						<td>{{ $post->updated_at }}</td>
						<td>
							<div class="btn-group btn-group-sm">
								<a class="btn btn-info" href="{{ URL::to('admin/posts/' . $post->id) }}" data-toggle="tooltip" data-placement="top" title="Toon gegevens"><i class="fa fa-info"></i></a>
								<a class="btn btn-primary" href="{{ URL::to('admin/posts/' . $post->id . '/edit') }}" data-toggle="tooltip" data-placement="top" title="Wijzig gegevens"><i class="fa fa-pencil"></i></a>
							</div>
							<div class="pull-right">
								<!-- delete the nerd (uses the destroy method DESTROY /nerds/{id} -->
								<!-- we will add this later since its a little more complicated than the other two buttons -->
								@if (Auth::check())
									{!! Form::open([
							            'method' => 'DELETE',
							            'route' => ['admin.posts.destroy', $post->id]]) 
							        !!}
							            {!! Form::button('<i class="fa fa-trash-o"></i>', 
							            	[	'type' => 'submit',
							            		'class' => 'btn btn-danger',
							            		'onclick' => 'return confirm("Verwijderen: Ben je zeker?")'
							            	]) 
							            !!}
							        {!! Form::close() !!}
								@endif
							</div>
							<div class="row">
							<div class="col-xs-12">
							@if ($post->active == 0)
								<a class="btn btn-success" href="{{ URL::to('admin/posts/' . $post->id . '/activate') }}" data-toggle="tooltip" data-placement="top" title="Stel publiek"><i class="fa fa-eye"></i></a>
							@else
								<a class="btn btn-danger" href="{{ URL::to('admin/posts/' . $post->id . '/deactivate') }}" data-toggle="tooltip" data-placement="top" title="Verberg"><i class="fa fa-eye-slash"></i></a>
							@endif
							</div>
							</div>
						</td>
					</tr>
					@endforeach
			</tbody>
		</table>
		{!! $posts->render() !!}
	</div>
</div>
<!-- ./ div -->

@stop
