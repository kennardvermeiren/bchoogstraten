@extends('layout.master')

@section('css')
	{!! Html::style('assets/css/image-picker.css') !!}
@endsection

@section('js')
{!! Html::script('assets/js/image-picker.min.js') !!}
<script type="text/javascript">
	$(document).ready(function () {
	    $("select").imagepicker();
	});
</script>
@endsection

{{-- Content --}}
@section('content')
@include('admin.albums.partials.navigation')

<!-- div -->
<div class="row">
	<div class="col-xs-12">
		<h2><strong>{{ trans('album.title') }}: </strong>{{{ $album->title }}}</h2>
		@if($album->images->count())
		{!! Form::open(array('url' => 'admin/albums/' . $album->id . '/remove')) !!}
			<select id="images" name="images[]" multiple="multiple" class="image-picker">
			@foreach($images as $image)
				<option 
				data-img-src='{!!asset('assets/gallery/' . $album->id . '/lowres/' . $image->filename . '.' . $image->mime)!!}'
				value='{!!$image->id!!}'
				></option>
			@endforeach
			</select>

			<div class="form-actions form-group">
		        {!! Form::submit('Submit', array('class' => 'btn btn-primary')) !!}
		    </div>

		{!! Form::close() !!}
		@else
			<div class="alert alert-warning" role="alert">
				Dit album bevat geen foto's!
			</div>
		@endif
	</div>
</div>
<!-- ./ div -->

@stop
