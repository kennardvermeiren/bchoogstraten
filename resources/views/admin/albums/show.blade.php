@extends('layout.master')

{{-- Content --}}
@section('content')
@include('admin.albums.partials.navigation')

<!-- div -->
<div class="row">
	<div class="col-xs-12">
		<a class="btn btn-default btn-lg btn-block" href="{{ URL::to('admin/albums/' . $album->id . '/edit') }}" data-toggle="tooltip" data-placement="top" title="Wijzig gegevens"><span class="glyphicon glyphicon-pencil"></span> Wijzigen</a>
		<h1><strong>{!! trans('album.title') !!}:</strong> {{{ $album->title }}}</h1>
		<h2><strong>{!! trans('album.summary') !!}:</strong></h2>
		<p>
			{!! $album->summary !!}
		</p>
		<h2><strong>{{{ trans('album.body') }}}:</strong></h2>
		<p>
			{!! $album->body !!}
		</p>
		@foreach($images as $image)
		<div class="col-xs-12 col-md-4">
			{!! Html::image(
				asset('assets/gallery/' . $album->id . '/lowres/' . $image->filename . '.' . $image->mime),
				'Gallery picture',
				array('class' => 'img-thumbnail img-responsive')
				) 
			!!}
		</div>
		@endforeach
	</div>
</div>
<!-- ./ div -->

@stop
