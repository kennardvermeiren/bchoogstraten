@extends('layout.master')

{{-- Content --}}
@section('content')
@include('admin.albums.partials.navigation')

<!-- div -->
<div class="row">
	<div class="col-xs-12">
		<table id="albums" class="table table-striped table-hover">
			<thead>
				<tr>
					<th class="col-xs-3">{{{ trans('album.title') }}}</th>
					<th class="col-xs-5">{{{ trans('album.summary') }}}</th>
					<th class="col-xs-1">{{ trans('album.image_amount') }}</th>
					<th class="col-xs-2">{{{ trans('admin.table_actions') }}}</th>
				</tr>
			</thead>
			<tbody>
				@foreach($albums as $album)
					<tr>
						<td>{{ $album->title }}</td>
						<td>{{ str_limit(strip_tags($album->summary, 250)) }}</td>
						<td>{{ $album->images->count() }}</td>
						<td>
							<div class="btn-group">
								<a class="btn btn-sm btn-info" href="{{ URL::to('admin/albums/' . $album->id) }}" data-toggle="tooltip" data-placement="top" title="Toon gegevens"><i class="fa fa-info"></i></a>
								<a class="btn btn-sm btn-primary" href="{{ URL::to('admin/albums/' . $album->id . '/edit') }}" data-toggle="tooltip" data-placement="top" title="Wijzig gegevens"><i class="fa fa-pencil"></i></a>
							</div>
							<div class="pull-right">
								<!-- delete the nerd (uses the destroy method DESTROY /nerds/{id} -->
								<!-- we will add this later since its a little more complicated than the other two buttons -->
								@if (Auth::check())
									{!! Form::open([
							            'method' => 'DELETE',
							            'route' => ['admin.albums.destroy', $album->id]]) 
							        !!}
							            {!! Form::button('<i class="fa fa-trash-o"></i>', 
							            	[	'type' => 'submit',
							            		'class' => 'btn btn-danger',
							            		'onclick' => 'return confirm("Verwijderen: Ben je zeker?")'
							            	]) 
							            !!}
							        {!! Form::close() !!}
								@endif
							</div>
							<div class="row">
								<div class="col-xs-12">
									<div class="btn-group">
										<a class="btn btn-sm btn-success" href="{{ URL::to('admin/albums/' . $album->id . '/add') }}" data-toggle="tooltip" data-placement="top" title="Upload foto's"><i class="fa fa-plus"></i></a>
										<a class="btn btn-sm btn-danger" href="{{ URL::to('admin/albums/' . $album->id . '/remove') }}" data-toggle="tooltip" data-placement="top" title="Verwijder foto's"><i class="fa fa-times"></i></a>
									</div>
								</div>
							</div>
							<div class="row">
							<div class="col-xs-12">
							@if ($album->active == 0)
								<a class="btn btn-success" href="{{ URL::to('admin/albums/' . $album->id . '/activate') }}" data-toggle="tooltip" data-placement="top" title="Stel publiek"><i class="fa fa-eye"></i></a>
							@else
								<a class="btn btn-danger" href="{{ URL::to('admin/albums/' . $album->id . '/deactivate') }}" data-toggle="tooltip" data-placement="top" title="Verberg"><i class="fa fa-eye-slash"></i></a>
							@endif
							</div>
							</div>
						</td>
					</tr>
					@endforeach
			</tbody>
		</table>
		{!! $albums->render() !!}
	</div>
</div>
<!-- ./ div -->

@stop
