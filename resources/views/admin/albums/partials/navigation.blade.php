<div class="page-header">
	<div class="pull-right">
		<a class="btn btn-default" href="{{ URL::to('admin/albums') }}"><i class="fa fa-list"></i><span class="hidden-xs"> Overzicht</span></a>
		<a class="btn btn-default" href="{{ URL::to('admin/albums/create') }}"><i class="fa fa-plus-square"></i></span><span class="hidden-xs"> Nieuw</span></a>
	</div>
</div>