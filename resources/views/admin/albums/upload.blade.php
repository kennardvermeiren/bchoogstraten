@extends('layout.master')

@section('css')
	{!! Html::style('assets/css/dropzone.css') !!}
@endsection

@section('js')
	{!! Html::script('assets/js/dropzone.js') !!}
@endsection

@section('scripts')
	<script type="text/javascript">
		var token = $('meta[name="csrf-token"]').attr('content');

		Dropzone.options.myAwesomeDropzone = {
		    paramName : 'file',
		    maxFilesize : 8, // Mo
		    acceptedFiles : 'image/*',
		    headers : {
		        'X-CSRF-TOKEN' : token
		    },
		    sending : function(file, xhr, formData) {
		        formData.append('id', $('form input[name=id]').val()); // Still a correct value here
		    },
		    success : function(file, response) {
		        console.log(response); // Will display the Request object (see controller)
		    },
		    error : function(file, error) {
		        console.error(error);
		    }
		}
	</script>
@endsection

{{-- Content --}}
@section('content')
@include('admin.albums.partials.navigation')

<!-- div -->
<div class="row">
	<div class="col-xs-12">
		{!! Form::open(['url' => '/admin/albums/' . $album->id. '/uploaded', 'class' => 'dropzone', 'id' => 'myAwesomeDropzone']) !!}
			    {!! Form::hidden('id', $album->id) !!}
		{!! Form::close() !!}
		<h2><strong>{{ trans('album.title') }}: </strong>{{{ $album->title }}}</h2>
	</div>
</div>
<!-- ./ div -->

@stop
