@extends('layout.master')

@section('css')
	{!! Html::style('assets/css/summernote.css') !!}
@endsection

@section('js')
	{!! Html::script('assets/js/summernote.js') !!}
	<script type="text/javascript">
		// initialise editor
		//var $summernote = $('#summernote');
		$(document).ready(function() {
			$('#summary').summernote({
				height: 100,
				toolbar: [
				    ['style', ['bold', 'italic', 'underline', 'clear']],
				    ['font', ['strikethrough', 'superscript', 'subscript']],
				    ['fontsize', ['fontsize']],
				    ['color', ['color']],
				    ['para', ['ul', 'ol', 'paragraph']],
				    ['insert', ['link', 'video', 'table', 'hr']]
				]
			});
			$('#body').summernote({
				height: 300,
				toolbar: [
				    ['style', ['bold', 'italic', 'underline', 'clear']],
				    ['font', ['strikethrough', 'superscript', 'subscript']],
				    ['fontsize', ['fontsize']],
				    ['color', ['color']],
				    ['para', ['ul', 'ol', 'paragraph']],
				    ['insert', ['link', 'video', 'table', 'hr']]
				]
			});
		});
	</script>
@endsection

{{-- Content --}}
@section('content')
@include('admin.albums.partials.navigation')

<!-- div -->
<div class="row">
	<div class="col-xs-12">
		{!! Form::model($album, ['method' => 'PATCH', 'route' => ['admin.albums.update', $album->id]]) !!}
		
		<div class="row">
			<div class="form-group @if ($errors->has('title')) has-error @endif">
				<div class="col-xs-10">
					{!! Form::label('title', trans('album.title')) !!}
					{!! Form::text('title', Input::old('title'), array('class' => 'form-control')) !!}
					@if ($errors->has('title')) <span class="help-block">{!! $errors->first('title') !!}</span> @endif
				</div>
			</div>
				
			<div class="form-group @if ($errors->has('active')) has-error @endif">
				<div class="col-xs-2">
					{!! Form::label('active', trans('album.active')) !!}
					<input type="checkbox" name="active" value="1" id="active" class="form-control" {!! $album->active ? "checked" : "" !!}>
					@if ($errors->has('active')) <span class="help-block">{!! $errors->first('active') !!}</span> @endif
				</div>
			</div>
		</div>

		<div class="row">
		<div class="form-group @if ($errors->has('summary')) has-error @endif">
			<div class="col-xs-12">
				{!! Form::label('summary', trans('album.summary')) !!}
				{!! Form::textarea('summary', Input::old('summary'), array('class' => 'summernote form-control', 'id' => 'summary')) !!}
				@if ($errors->has('summary')) <span class="help-block">{!! $errors->first('summary') !!}</span> @endif
			</div>
		</div>
		</div>
		
		<div class="row">
		<div class="form-group @if ($errors->has('body')) has-error @endif">
			<div class="col-xs-12">
				{!! Form::label('body', trans('album.body')) !!}
				{!! Form::textarea('body', Input::old('body'), array('class' => 'summernote form-control', 'id' => 'body')) !!}
				@if ($errors->has('body')) <span class="help-block">{!! $errors->first('body') !!}</span> @endif
			</div>
		</div>
		</div>

		<div class="form-group form-actions">
			{!! Form::submit('Submit', array('class' => 'btn btn-primary')) !!}
		</div>

	{!! Form::close() !!}
	</div>
</div>
<!-- ./ div -->

@stop
