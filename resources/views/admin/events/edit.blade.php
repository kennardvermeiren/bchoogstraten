@extends('layout.master')

@section('css')
	{!! Html::style('assets/css/summernote.css') !!}
@endsection

@section('js')
	{!! Html::script('assets/js/summernote.js') !!}
	<script type="text/javascript">
		$('#body').summernote({
			height: 300,
			toolbar: [
			    ['style', ['bold', 'italic', 'underline', 'clear']],
			    ['font', ['strikethrough', 'superscript', 'subscript']],
			    ['fontsize', ['fontsize']],
			    ['color', ['color']],
			    ['para', ['ul', 'ol', 'paragraph']],
			    ['insert', ['link', 'video', 'table', 'hr']]
			]
		});
	</script>
@endsection

{{-- Content --}}
@section('content')
@include('admin.events.partials.navigation')

<!-- div -->
<div class="row">
	<div class="col-xs-12">
		{!! Form::model($event, ['method' => 'PATCH', 'route' => ['admin.events.update', $event->id]]) !!}
		<div class="row">
			<div class="form-group @if ($errors->has('title')) has-error @endif">
				<div class="col-xs-10">
					{!! Form::label('title', trans('event.title')) !!}
					{!! Form::text('title', null, array('class' => 'form-control')) !!}
					@if ($errors->has('title')) <span class="help-block">{!! $errors->first('title') !!}</span> @endif
				</div>
			</div>

			<div class="form-group @if ($errors->has('active')) has-error @endif">
				<div class="col-xs-2">
					{!! Form::label('active', trans('event.active')) !!}
					<input type="checkbox" name="active" value="1" id="active" class="form-control" {!! $event->active ? "checked" : "" !!}>
					@if ($errors->has('active')) <span class="help-block">{!! $errors->first('active') !!}</span> @endif
				</div>
			</div>
		</div>

		<div class="row">
			<div class="form-group col-xs-12 col-md-4 @if ($errors->has('begin')) has-error @endif">
	            {!! Form::label('begin', trans('event.begin')) !!}
	            {!! Form::input('datetime-local', 'begin', $event->getBeginHTML5() ,array('class' => 'form-control')) !!}
	            @if ($errors->has('begin')) <span class="help-block">{!! $errors->first('begin') !!}</span> @endif
	        </div>
	        <div class="form-group col-xs-12 col-md-4 @if ($errors->has('end')) has-error @endif">
	            {!! Form::label('end', trans('event.end')) !!}
	            {!! Form::input('datetime-local', 'end', $event->getEndHTML5() ,array('class' => 'form-control')) !!}
	            @if ($errors->has('end')) <span class="help-block">{!! $errors->first('end') !!}</span> @endif
	        </div>
	    </div>

	    <div class="row">
	    	<div class="form-group @if ($errors->has('location')) has-error @endif">
				<div class="col-xs-10">
					{!! Form::label('location', trans('event.location')) !!}
					{!! Form::text('location', Input::old('location'), array('class' => 'form-control')) !!}
					@if ($errors->has('location')) <span class="help-block">{!! $errors->first('location') !!}</span> @endif
				</div>
			</div>
	    </div>

		<div class="row">
			<div class="form-group @if ($errors->has('body')) has-error @endif">
				<div class="col-xs-12">
					{!! Form::label('body', trans('event.body')) !!}
					{!! Form::textarea('body', null, array('class' => 'summernote form-control', 'id' => 'body')) !!}
					@if ($errors->has('body')) <span class="help-block">{!! $errors->first('body') !!}</span> @endif
				</div>
			</div>
		</div>

		<div class="form-group form-actions">
			{!! Form::submit('Submit', array('class' => 'btn btn-primary')) !!}
		</div>

	{!! Form::close() !!}
	</div>
</div>
<!-- ./ div -->

@stop
