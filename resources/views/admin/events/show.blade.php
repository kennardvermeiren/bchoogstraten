@extends('layout.master')

{{-- Content --}}
@section('content')
@include('admin.events.partials.navigation')

<!-- div -->
<div class="row">
	<div class="col-xs-12">
		<a class="btn btn-default btn-lg btn-block" href="{{ URL::to('admin/events/' . $event->id . '/edit') }}" data-toggle="tooltip" data-placement="top" title="Wijzig gegevens"><span class="glyphicon glyphicon-pencil"></span> Wijzigen</a>
		<h1><strong>{{{ trans('event.title') }}}:</strong> {!! $event->title !!}</h1>
		<div class="col-xs-12 col-md-6">
			<p class="lead">Van: <br>
				<i class="fa fa-calendar-o"></i> {{ $event->begin->format('d/m/y') }} <br>
				<i class="fa fa-clock-o"></i> {{ $event->begin->format('H:i') }}
			</p>
		</div>
		<div class="col-xs-12 col-md-6">
			<p class="lead">Tot: <br>
				<i class="fa fa-calendar-o"></i> {{ $event->end->format('d/m/y') }} <br>
				<i class="fa fa-clock-o"></i> {{ $event->end->format('H:i') }}
			</p>
		</div>
		<h2><strong>{{ trans('event.location') }}:</strong></h2> 
		<p class="lead">{!! $event->location !!}</p>
		<h2><strong>{{{ trans('event.body') }}}:</strong></h2>
		<p>
			{!! $event->body !!}
		</p>
	</div>
</div>
<!-- ./ div -->

@stop
