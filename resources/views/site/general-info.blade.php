@extends('layout.master')

@section('title', 'Algemene info')

{{-- Content --}}
@section('content')
<div class="row">
    <div class="col-xs-12">
        <h1>Speeluren</h1>

        <table class="table">
            <thead>
                <tr>
                    <th></th>
                    <th>Woensdag</th>
                    <th>Zaterdag</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th>Jeugd</th>
                    <td>18.30u - 20.00u (training)</td>
                    <td>10.00u - 12.00u (vrij spel)</td>
                </tr>
                <tr>
                    <th>Volwassenen</th>
                    <td>20.00u - 22.30u</td>
                    <td>10.00u - 12.00u (vrij spel)</td>
                </tr>
                <tr>
                    <th>Minibad</th>
                    <td></td>
                    <td>10.00u - 11.00u</td>
                </tr>
                <tr>
                    <th>G-Badminton</th>
                    <td></td>
                    <td>11.00u - 12.00u</td>
                </tr>
            </tbody>
        </table>

    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <h1>Locatie sporthal</h1>
        <adress>
            <strong>Sporthal: De Zevensprong (Spijker VTI)</strong><br>
            Gelmelstraat 62<br>
            2320 Hoogstraten<br>
            Informatie/klachten/vragen: {!!Html::mailto('zaalverantwoordelijke@bchoogstraten.be', 'zaalverantwoordelijke@bchoogstraten.be')!!}
        </adress>
        <div class="embed-responsive embed-responsive-16by9">
        <iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2488.972858343448!2d4.7681728!3d51.40355340000001!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x6f2ac77a3d9fb62b!2sSporthal+Zevensprong!5e0!3m2!1sen!2sbe!4v1408551513653"></iframe>
        </div>
    </div>
</div>

@stop
