@extends('layout.master')

@section('title', 'Documenten')

{{-- Content --}}
@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="page-header">
            <h1>Documenten:</h1>
        </div>
    </div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-4">
        <a href="{{ URL::to('assets/site/docs/clubreglement.pdf') }}" target="_blank" class="btn btn-default btn-block btn-lg"><i class="fa fa-file-pdf-o fa-2x"></i> Clubreglement</a>
        
    </div>
    <div class="col-xs-12 col-sm-4">
        <a href="{{ URL::to('assets/site/docs/onthaalbrochure_jeugd.pdf') }}" target="_blank" class="btn btn-default btn-block btn-lg"><i class="fa fa-file-pdf-o fa-2x"></i> Onthaalbrochure Jeugd</a>
        
    </div>
    <div class="col-xs-12 col-sm-4">
        <a href="{{ URL::to('assets/site/docs/spelregels_badminton.pdf') }}" target="_blank" class="btn btn-default btn-block btn-lg"><i class="fa fa-file-pdf-o fa-2x"></i> Spelregels</a>
        
    </div>
    <div class="col-xs-12 col-sm-4">
        <a href="{{ URL::to('assets/site/docs/informatienota.pdf') }}" target="_blank" class="btn btn-default btn-block btn-lg"><i class="fa fa-file-pdf-o fa-2x"></i> Vrijwilligers</a>
        
    </div>
    <div class="col-xs-12 col-sm-4">
        <a href="{{ URL::to('assets/site/docs/panathlon_verklaring.pdf') }}" target="_blank" class="btn btn-default btn-block btn-lg"><i class="fa fa-file-pdf-o fa-2x"></i> Panathlon verklaring</a>
        
    </div>
    <div class="col-xs-12 col-sm-4">
        <a href="{{ URL::to('assets/site/docs/kvo_schoolkalender2014.pdf') }}" target="_blank" class="btn btn-default btn-block btn-lg"><i class="fa fa-file-pdf-o fa-2x"></i> KVO Schoolkalender 2014</a>
        
    </div>
</div>

@stop
