@extends('layout.master')

@section('title', 'Contact')

{{-- Content --}}
@section('content')
<div class="row">
	<div class="col-xs-12">
		<div class="page-header">
			<h1>Contact</h1>
		</div>
		<p>Heb je een vraag? Contacteer ons via dit formulier.</p>
	</div>
</div>


{!! Form::open(array('url' => 'contact-sent')) !!}
<div class="row">
	<div class="form-group col-xs-12 col-sm-6 @if ($errors->has('email')) has-error @endif">
	{!! Form::label('email', trans('forms.field.email')) !!}
	{!! Form::email('email', Input::old('email'), array('class' => 'form-control')) !!}
	@if ($errors->has('email')) <span class="help-block">{!! $errors->first('email') !!}</span> @endif
	</div>
</div>
<div class="row">
	<div class="form-group col-xs-12 @if ($errors->has('message')) has-error @endif">
	{!! Form::label('message', trans('forms.field.message')) !!}
	{!! Form::textarea('message', Input::old('message'), array('class' => 'form-control')) !!}
	@if ($errors->has('message')) <span class="help-block">{!! $errors->first('message') !!}</span> @endif
	</div>
</div>
{!! app('captcha')->display(); !!}
<br/>
{!! Form::submit(trans('forms.button.submit'), array('class' => 'btn btn-primary')) !!}

{!! Form::close() !!}

@stop
