@extends('layout.master')

@section('title', $post->title)

@section('meta')
<meta property="og:url" 				content="{!! Request::url() !!}" />
<meta property="og:type" 				content="article" />
<meta property="og:locale" 				content="nl_BE" />
<meta property="og:site_name" 			content="Badminton club Hoogstraten" />
<meta property="og:title" 				content="{!! $post->title !!}" />
<meta property="og:description" 		content="{{ preg_replace('/(<.*?>)|(&.*?;)/', '', $post->summary) }}" />
<meta property="og:image" 				content="{{{ asset('assets/site/logo_bch.png') }}}" />
@endsection

{{-- Content --}}
@section('content')

<!-- div -->
<div class="row">
	<div class="col-xs-12">
		<div class="page-header">
			<h1>{{ $post->title }}</h1>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
	    <i class="fa fa-clock-o"></i> {{ $post->updated_at->diffForHumans() }}
	    <br>
		@if($post->edited())
			<em><small>(Bewerkt)</small></em>
		@endif
	    <div class="pull-right"><i class="fa fa-user"></i> {!! $post->user->first_name !!}</div>
	    <hr>
	    <div>
	    	{!! $post->body !!}
	    </div>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-xs-2">
		<button class="btn btn-primary" onclick="history.go(-1)">
	      « Terug
	    </button>
	</div>
	<div class="col-xs-10">
	    <a class="btn share-btn share-btn-facebook" href="http://www.facebook.com/sharer.php?u={!!Request::url()!!}" target="_blank">
		    <i class="fa fa-facebook"></i>
		</a>
		<a class="btn share-btn share-btn-twitter" href="http://twitter.com/share?url={!!Request::url()!!}&text=Lees het nu&via=http://www.bchoogstraten.be" target="_blank">
		    <i class="fa fa-twitter"></i>
		</a>
		<a class="btn share-btn share-btn-google-plus" href="https://plus.google.com/share?url={!!Request::url()!!}" target="_blank">
		    <i class="fa fa-google-plus"></i>
		</a>
		<a class="btn share-btn share-btn-email" href="mailto:?subject={!! $post->title !!}&body={!! str_limit(strip_tags($post->content)) !!}" target="_blank">
		    <i class="fa fa-envelope"></i>
		</a>
	</div>
</div>
<!-- ./ div -->

@stop
