@extends('layout.master')

@section('js')
	{!! Html::script('assets/js/isotope.min.js') !!}
	{!! Html::script('assets/js/infinitescroll.min.js') !!}
	<script>
	$(document).ready(function(){
		$('.grid').isotope({
			itemSelector	: '.grid-item'
		});
	});
	</script>
@endsection

@section('title', config('calendar.title'))

{{-- Content --}}
@section('content')

<!-- div -->
<div class="row">
	<div class="col-xs-12">
		<div class="page-header">
			<h1>{{ config('calendar.title') }}</h1>
		</div>
	</div>
</div>
<div class="grid row">
	<div class="col-xs-12">
	@foreach ($events as $event)
		<a href="/calendar/{{ $event->slug }}">
			<div class="panel panel-default unstyled-link grid-item">
				<div class="panel-heading">
					<h1 class="panel-title">{!! $event->title !!}</h1>
				</div>
				<div class="panel-body">
					<div class="col-xs-12 col-md-6">
						Van:
							<p class="lead">
								<i class="fa fa-calendar-o"></i> {{ $event->begin->format('d/m/y') }} <br>
								<i class="fa fa-clock-o"></i> {{ $event->begin->format('H:i') }}
							</p>
					</div>
					<div class="col-xs-12 col-md-6">
						Tot:
							<p class="lead">
								<i class="fa fa-calendar-o"></i> {{ $event->end->format('d/m/y') }} <br>
								<i class="fa fa-clock-o"></i> {{ $event->end->format('H:i') }}
							</p>
					</div>
					Meer info... <i class="fa fa-arrow-circle-o-right"></i>
				</div>
			</div>
		</a>
      @endforeach
      </div>
</div>
<div class="row">
	<div class="col-xs-12">
		{!! $events->render() !!}
	</div>
</div>
<!-- ./ div -->

@stop
