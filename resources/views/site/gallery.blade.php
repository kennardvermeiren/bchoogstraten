@extends('layout.master')

@section('js')
	{!! Html::script('assets/js/isotope.min.js') !!}
	{!! Html::script('assets/js/infinitescroll.min.js') !!}
	<script>
	$(document).ready(function(){
		$('.grid').isotope({
			itemSelector	: '.grid-item'
		});
	});
	</script>
@endsection

@section('title', config('gallery.title'))

{{-- Content --}}
@section('content')

<!-- div -->
<div class="row">
	<div class="col-xs-12">
		<div class="page-header">
			<h1>{{ config('gallery.title') }}</h1>
		</div>
	</div>
</div>
<div class="grid row">
	<div class="col-xs-12">
	@foreach ($albums as $album)
		<a href="/gallery/{{ $album->slug }}">
			<div class="panel panel-default unstyled-link grid-item">
				<div class="panel-heading">
					<h1 class="panel-title">{!! $album->title !!}</h1>
				</div>
				<div class="panel-body">
					<i class="fa fa-clock-o"></i> {{ $album->updated_at->diffForHumans() }}
					<br>
					@if($album->edited())
						<em><small>(Bewerkt)</small></em>
					@endif
					<div class="pull-right">
						<i class="fa fa-user"></i> {!! $album->user->first_name !!}
					</div>
					<hr>
					<p>
						{!! $album->summary !!}
						@foreach($album->images->take(1) as $image)
							{!! Html::image(
								asset('assets/gallery/' . $album->id . '/lowres/' . $image->filename . '.' . $image->mime),
								'Gallery picture',
								array(	'class' => 'img-responsive'
									)
								) 
							!!}
						@endforeach
					</p>
					<hr>
					<p>Lees meer... <i class="fa fa-arrow-circle-o-right"></i></p>
				</div>
			</div>
		
		</a>
      @endforeach
      </div>
</div>
<div class="row">
	<div class="col-xs-12">
		{!! $albums->render() !!}
	</div>
</div>
<!-- ./ div -->

@stop
