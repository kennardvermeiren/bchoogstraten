@extends('layout.master')

@section('title', $event->title)

{{-- Content --}}
@section('content')

<!-- div -->
<div class="row">
	<div class="col-xs-12">
		<div class="page-header">
			<h1>{!! $event->title !!}</h1>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-md-6">
		<p class="lead">Van: <br>
			<i class="fa fa-calendar-o"></i> {{ $event->begin->format('d/m/y') }} <br>
			<i class="fa fa-clock-o"></i> {{ $event->begin->format('H:i') }}
		</p>
	</div>
	<div class="col-xs-12 col-md-6">
		<p class="lead">Tot: <br>
			<i class="fa fa-calendar-o"></i> {{ $event->end->format('d/m/y') }} <br>
			<i class="fa fa-clock-o"></i> {{ $event->end->format('H:i') }}
		</p>
	</div>
	<div class="col-xs-12">
    	{!! $event->body !!}
    </div>
    <hr>
    <div class="col-xs-12">
	    <button class="btn btn-primary" onclick="history.go(-1)">
	      « Terug
	    </button>
	</div>
</div>

@stop
