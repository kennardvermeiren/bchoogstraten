@extends('layout.master')

@section('css')
	{!! Html::style('assets/css/lightbox.css') !!}
@endsection

@section('js')
	{!! Html::script('assets/js/isotope.min.js') !!}
	{!! Html::script('assets/js/lightbox.js') !!}
	<script type="text/javascript">
	$(window).load(function(){
		lightbox.option({
			'resizeDuration': 400,
		});
		$('.grid').isotope({
			itemSelector	: '.grid-item'
		});
	});
	</script>
@endsection

@section('title', $album->title)

{{-- Content --}}
@section('content')

<!-- div -->
<div class="row">
	<div class="col-xs-12">
		<div class="page-header">
			<h1>{!! $album->title !!}</h1>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
	    <i class="fa fa-clock-o"></i> {{ $album->updated_at->diffForHumans() }}
		<br>
		@if($album->edited())
			<em><small>(Bewerkt)</small></em>
		@endif
		<div class="pull-right">
			<i class="fa fa-user"></i> {!! $album->user->first_name !!}
		</div>
		<hr>
    	<div>
	    	{!! $album->body !!}
		</div>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-xs-2">
		<button class="btn btn-primary" onclick="history.go(-1)">
	      « Terug
	    </button>
	</div>
</div>
@if ($images->count() == 0)
	<p class="lead">Dit album is momenteel leeg.</p>
@else
<div class="grid row">
	<div class="col-xs-12">
    @foreach($images as $image)
	<div class="grid-item col-xs-12 col-md-4">
		<a href="{!! asset('assets/gallery/' . $album->id . '/hires/' . $image->filename . '.' . $image->mime) !!}" data-lightbox="{{ $album->title }}">
			{!! Html::image(
				asset('assets/gallery/' . $album->id . '/lowres/' . $image->filename . '.' . $image->mime),
				'Gallery picture',
				array(	'class' => 'img-responsive'
					)
				) 
			!!}
		</a>
	</div>
	@endforeach
	</div>
</div>
<div class="col-xs-12">
	{!! $images->render() !!}
</div>
@endif
<!-- ./ div -->

@stop
