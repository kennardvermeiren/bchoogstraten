@extends('layout.master')

@section('title', 'Over')

{{-- Content --}}
@section('content')

<!-- div -->
<div class="row">
	<div class="col-xs-12">
		<div class="page-header">
			<h1>BC Hoogstraten:</h1>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<p>
		Welkom op de site van Badmintonclub Hoogstraten! Op deze site vinden jullie alle informatie en nieuwigheden terug. 
		Voor vragen en / of meer informatie kan u altijd terecht bij een van onze bestuursleden. 
		Verder bent u altijd welkom op een van onze speelmomenten in onze sporthal. Tot dan!
		</p>
	</div>
</div>
<!-- ./ div -->
<!-- div -->
<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-default">
			<div class="panel-heading"><h3 id="board">Bestuur</h3></div>
			<div class="panel-body">
				<p>Hieronder vindt u alle leden van het bestuur. Contact opnemen kan via het bijstaande emailadres.</p>
			</div>
			<table class="table">
				<thead>
			        <tr>
			        	<th>Functie</th>
			            <th>Naam</th>
			            <th>Email</th>
			        </tr>
			    </thead>
			    <tbody>
			        <tr>
			            <td>Voorzitter</td>
			            <td>Pieter Coenegrachts</td>
			            <td>voorzitter@bchoogstraten.be</td>
			        </tr>
			        <tr>
			            <td>Secretaris</td>
			            <td>Robin Jansen</td>
			            <td>secretariaat@bchoogstraten.be</td>
			        </tr>
			        <tr>
			            <td>Penningmeester</td>
			            <td>Nathalie Geets</td>
			            <td>penningmeester@bchoogstraten.be</td>
			        </tr>
			        <tr>
			            <td>Competitieverantwoordelijke</td>
			            <td>Pieter Coenegrachts</td>
			            <td>competitie@bchoogstraten.be</td>
			        </tr>
			        <tr>
			            <td>Jeugdcoördinator (Trainers, Joc, Mini-bad & G-badminton)</td>
			            <td>Gwen Anthonissen</td>
			            <td>jeugd@bchoogstraten.be</td>
			        </tr>
			        <tr>
			            <td>Materiaal - en zaalverantwoordelijke</td>
			            <td>Thijs Vervoort</td>
			            <td>zaalverantwoordelijke@bchoogstraten.be</td>
			        </tr>
			        <tr>
			            <td>Ledenadministratie</td>
			            <td>Pieter Coenegrachts</td>
			            <td>leden@bchoogstraten.be</td>
			        </tr>
			        <tr>
			            <td>PR (Quiz, Fuif)</td>
			            <td>Koen Cleymans</td>
			            <td>pr@bchoogstraten.be</td>
			        </tr>
			        <tr>
			            <td>Website</td>
			            <td>Thijs Vervoort</td>
			            <td>website@bchoogstraten.be</td>
			        </tr>
                    <tr>
			        	<td></td>
			        	<td>Rudi Van Gastel</td>
			        	<td>rudi@bchoogstraten.be</td>
			        </tr>
			    </tbody>
			</table>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-xs-6 col-md-4">
		<div class="thumbnail">
			{!! Html::image(asset('assets/site/staff/pieter_coenegrachts.jpg')) !!}
			<div class="caption">
				<h3>Pieter Coenegrachts</h3>
			</div>
		</div>
	</div>
	<div class="col-xs-6 col-md-4">
		<div class="thumbnail">
			{!! Html::image(asset('assets/site/staff/robin_jansen.jpg')) !!}
			<div class="caption">
				<h3>Robin Jansen</h3>
			</div>
    	</div>
	</div>
	<div class="col-xs-6 col-md-4">
		<div class="thumbnail">
			{!! Html::image(asset('assets/site/staff/koen_cleymans.jpg')) !!}
			<div class="caption">
				<h3>Koen Cleymans</h3>
			</div>
		</div>
	</div>
	<div class="col-xs-6 col-md-4">
		<div class="thumbnail">
			{!! Html::image(asset('assets/site/staff/nathalie_geets.jpg')) !!}
			<div class="caption">
				<h3>Nathalie Geets</h3>
			</div>
		</div>
	</div>
	<div class="col-xs-6 col-md-4">
		<div class="thumbnail">
			{!! Html::image(asset('assets/site/staff/thijs_vervoort.jpg')) !!}
			<div class="caption">
				<h3>Thijs Vervoort</h3>
			</div>
		</div>
	</div>
	<div class="col-xs-6 col-md-4">
		<div class="thumbnail">
			{!! Html::image(asset('assets/site/staff/gwen_anthonissen.jpg')) !!}
			<div class="caption">
				<h3>Gwen Anthonissen</h3>
			</div>
		</div>
	</div>
    <div class="col-xs-6 col-md-4">
		<div class="thumbnail">
			{!! Html::image('https://placehold.it/200x250?text=?') !!}
			<div class="caption">
				<h3>Rudi Van Gastel</h3>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 bg-danger">
		<h2 id="payment">Betalingsgegevens</h2>
		<p class="lead">Rekeningnummer: BE64 8508 7669 1452<br/>Naam: Badmintonclub Hoogstraten vzw<br/>Adres: Witherenweg 35, 2320 Hoogstraten</p>
		<hr/>
		<h2 id="prices">Prijzen</h2>
		<table class="table">
			<thead>
		        <tr>
		        	<th>Categorie</th>
		            <th>Prijs</th>
		        </tr>
		    </thead>
		    <tbody>
		        @if($categories->count())
					@foreach($categories as $category)
					<tr>
						<td>{!! Lang::get('category.' . $category->name) !!}</td>
						<td>€{!! $category->price !!}</td>
					</tr>
					@endforeach
				@endif
		    </tbody>
		</table>
		<p class="lead"><u>Let op:</u> Bij de betaling van dit bedrag bent u ingeschreven tot en met december {{{date("Y")}}}. Vanaf seizoen 2016 gaan we mee met het seizoen van Badminton Vlaanderen, dat van januari tot december loopt.</p>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">
	<h2>Leden</h2>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">
		<h3 id="adults">Volwassenen</h3>
		<p>
			Recreant of competitiespeler? Jong volwassen (16+) of iets minder jong volwassen?<br/>
			Iedereen is welkom bij Badmintonclub Hoogstraten. Het seizoen begint op de eerste woensdag van september en loopt het ganse jaar door, met uitzondering van de maand juli. 
			We spelen in sporthal de Zevensprong (Gelmelstraat 62 , 2320 Hoogstraten) op woensdag van 20.00u tot 22.30u en zaterdag van 10.00u tot 12.00u. 
			In juni en augustus schakelen we over op een zomerregeling en wordt er enkel op woensdag gespeeld.<br/>
			Naast het badmintonnen organiseren we als club ook elk jaar enkele activiteiten (quiz, bbq, clubtornooien, teambuilding, familietornooi,….).<br/>
			Het lidgeld voor recreatieve leden bedraagt €@if($categories->count()){!! $categories->get(1)->price !!}@endif, en voor de competitieleden €@if($categories->count()){!! $categories->get(0)->price !!}@endif (Extra kost veren shuttles).<br/>
			Je bent steeds welkom op een van onze speelmomenten om kennis te maken met onze club. Tot dan!
		</p>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<h3 id="competition">Competitie</h3>
		<p>
			Sinds het seizoen 2009-2010 is BC Hoogstraten aangesloten bij Badminton Vlaanderen en vaardigen we twee competitieploegen af in de PBA-competitie. 
			Het gaat hierbij om een herenploeg (Hoogstraten 1H) en een gemengde ploeg (Hoogstraten 1G). Het lidgeld voor een competitiespeler bedraagt €@if($categories->count()){!! $categories->get(0)->price !!}@endif. 
			Voor meer informatie kan je mailen naar {!!Html::mailto('competitie@bchoogstraten.be', Html::email('competitie@bchoogstraten.be'))!!}.
		</p>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">
		<h3 id="youth">Jeugd</h3>
		<p>
			Badminton Hoogstraten heeft een grote jeugdafdeling van ongeveer 50 spelers. Hieronder verstaan wij alle kinderen van 9 tot 16 jaar. 
			Iedere woensdag krijgen zij van 18.30-20.00 training van gespecialiseerde initiatortrainers. We spelen in verschillende groepen, afhankelijk van de leeftijd en het niveau van de speler. 
			Ook op zaterdag kan er badminton gespeeld worden van 10.00-12.00 uur. Daar wordt geen training voorzien, maar kan er vrij gespeeld worden.<br/>
			Vanaf september 2013 is ieder lid automatisch ingeschreven bij Badminton Vlaanderen, waardoor zij de mogelijkheid hebben om mee te doen aan het PBA-circuit.<br/>
			Het seizoen start de eerste woensdag van september, maar er kan ook later in het jaar zonder probleem aangesloten worden. 
			Het lidgeld bedraagt €@if($categories->count()){!! $categories->get(2)->price !!}@endif voor het ganse seizoen, dat loopt van begin september tot het einde van mei. 
			Voor meer informatie kan u steeds contact opnemen via {!!Html::mailto('jeugd@bchoogstraten.be', 'jeugd@bchoogstraten.be')!!}.
		</p>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">
		<h3 id="minibad">Mini-bad</h3>
		<p>
			Voor de jongere spelers onder ons, van 3 tot 9 jaar, voorziet Badmintonclub Hoogstraten vzw een aangepast badmintonvorm, namelijk Minibad! 
			Dit is een variatie op badminton waarbij het veld verkleind wordt en het net lager hangt. De spelers krijgen een korter racket.<br/>
			Elke zaterdag wordt er van 10.00 uur - 11.00 uur een training voorzien door onze trainers Gwen Anthonissen, Joke Van Delm en Kathleen Geets. 
			Het lidgeld voor het volledige seizoen bedraagt €@if($categories->count()){!! $categories->get(3)->price !!}@endif. Voor meer informatie, kan u contact opnemen via {!!Html::mailto('jeugd@bchoogstraten.be', 'jeugd@bchoogstraten.be')!!}.
			Naast het aanleren van de badmintonvaardigheden, vinden wij het erg belangrijk dat de kinderen een basis krijgen van twaalf verschillende basisvaardigheden. 
			Daarom doen wij vanaf het 2015 mee aan Multimove, een gevarieerd bewegingsaanbod voor jonge kinderen. Voor meer info, verwijzen wij u graag naar de website: <a href="http://multimove.be/" target="_blank">multimove.be</a>
		</p>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">
		<h3 id="gbad">G-Badminton</h3>
		<p>
			Op 24 maart 2012 is Badmintonclub Hoogstraten vzw van start gegaan met het geven van G-badminton. Dit is badminton voor personen met een handicap. 
			De club richt zich specifiek op jeugdspelers tussen de 8 en 16 jaar met een verstandelijke beperking.<br/> 
			De sport wordt gegeven door Gwen Anthonissen en Joke Van Delm, beide jeugdtrainers bij de club. 
			Zij hebben hun schouders mee onder het project gezet en extra cursussen gevolgd voor de begeleiding van deze specifieke doelgroep.<br/>
			Er wordt elke week op zaterdag van 11u tot 12u een training gegeven. Voor meer info kan je terecht bij {!!Html::mailto('jeugd@bchoogstraten.be', 'jeugd@bchoogstraten.be')!!}. 
			Het lidgeld voor een volledig seizoen badminton bedraagt €@if($categories->count()){!! $categories->get(4)->price !!}@endif.
		</p>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">
		<h3 id="youthguide">Jeugdgids</h3>
		<a href="{!! URL::to('assets/site/docs/jeugdgids.pdf') !!}" target="_blank" class="btn btn-info btn-block btn-lg">Jeugdgids (PDF)</a>
		<br/>
		<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="headingOne">
					<h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
							KYU
						</a>
					</h4>
				</div>
				<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
					<div class="panel-body">
						Beste spelers, beste ouders <br/><br/>
						Graag willen wij jullie laten kennismaken met KYU. <br/>
						KYU is een ontwikkelingstraject voor jonge en beginnende badmintonners dat loopt via gekleurde badjes en de ontwikkelingslijn van badminton (van beginner tot gevorderde speler) volgt. <br/>
						Elk traject is in principe opgebouwd uit zes pijlers: theoretische kennis, algemene coördinatie en transferbewegingen, slagtechniek, keuzes en accenten, functionele (lichaams)houdingen, footwork en tacktiek. <br/><br/>
						Wij kiezen er als club voor om met het bandjessysteem te werken de behaalde graad ook door te geven in het ledenbestand. Deze verschillende kleurgradaties kunnen behaald worden na het afleggen van badmintonexamens. De trainingen die gegeven worden, zullen daarom ook aangepast worden waardoor het voor iedereen mogelijk is om een hoog niveau te behalen.<br/><br/>
						Wanneer iemand slaagt voor zijn examens vinden wij dit als club super. Daarom hebben we besloten om jullie de bandjes te geven als beloning i.p.v. ze door de leden te laten betalen. De behaalde graad komt ook in het ledenbestand van Badminton Vlaanderen terecht. <br/><br/>
						Iedere speler start met een wit bandje. We gaan de komende weken met verschillende onderwerpen aan de slag zodat zoveel mogelijk gemotiveerde leden de volgende graad, nl. het groene bandje behalen. <br/><br/>
						Wanneer de spelers al een behoorlijk niveau hebben, is het mogelijk om naar een rood bandje te werken tegen een volgende examenperiode. Het laatste doel is het blauwe bandje. Dit is de hoogst mogelijke graad die behaald kan worden en wensen we iedereen toe. Wie er hard voor werkt, zal beloond worden.<br/><br/>
						Hopelijk zijn jullie even enthousiast als wij. <br/><br/>
						Informatie over de examens volgt snel. Voor meer informatie over KYU kan je steeds terecht bij de trainers of een mailtje sturen naar {!!Html::mailto('jeugd@bchoogstraten.be', 'jeugd@bchoogstraten.be')!!}<br/>
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="headingTwo">
					<h4 class="panel-title">
						<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
						Multimove
						</a>
					</h4>
				</div>
				<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
					<div class="panel-body">
						Aangezien wij, het bestuur van Badmintonclub Hoogstraten vzw, het belangrijk vinden dat kinderen veel bewegen, hebben wij onze trainer Gwen, in mei een opleiding Multimove laten volgen.<br/><br/>
						Multimove staat voor bewegen. De bedoeling van deze trainingen is dat kinderen in contact komen met twaalf bewegingsvaardigheden. Deze zullen zij gedurende twintig lessen leren kennen waardoor zij deze vaardigheden ook beter ontwikkelen.<br/><br/>
						Het doel is om op die manier meer en jongere kinderen te bereiken. Hoe vroeger kinderen begeleid worden in hun sportieve ontwikkeling, des te meer kwaliteit wordt er bekomen, niet alleen op individuele badmintontechnische aspecten, maar ook breed maatschappelijke. Daarom kiezen wij ervoor om onze groep Minibadders hiermee in contact te brengen. De training Multimove zullen doorgaan op zaterdagen van 10.00-11.00 uur. Er zal een afwisseling gemaakt worden tussen Multimove en Minibad, waarbij iedere week iets anders wordt gedaan.<br/><br/>
						De exacte kalender kan u onderaan terugvinden. U kunt er dus voor kiezen om enkel deel te nemen aan de Multimove lessen, aan de badmintonlessen of om van beide gelegenheden te genieten.<br/>
						<table class="table table-striped">
							<thead>
								<tr>
									<th>Datum</th>
									<th>Beschrijving</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>19 september '15</td>
									<td>Minibad</td>
								</tr>
								<tr>
									<td>26 september '15</td>
									<td>Multimove</td>
								</tr>
								<tr>
									<td>3 oktober '15</td>
									<td>Multimove</td>
								</tr>
								<tr>
									<td>10 oktober '15</td>
									<td>Multimove</td>
								</tr>
								<tr>
									<td>17 oktober '15</td>
									<td>Minibad</td>
								</tr>
								<tr>
									<td>24 oktober '15</td>
									<td>Minibad</td>
								</tr>
								<tr>
									<td>31 oktober '15</td>
									<td>Multimove</td>
								</tr>
								<tr>
									<td>7 november '15</td>
									<td>Multimove</td>
								</tr>
								<tr>
									<td><strong>14 november '15</strong></td>
									<td><strong>Geen training</strong></td>
								</tr>
								<tr>
									<td>21 november '15</td>
									<td>Multimove</td>
								</tr>
								<tr>
									<td>28 november '15</td>
									<td>Minibad</td>
								</tr>
								<tr>
									<td>5 december '15</td>
									<td>Multimove</td>
								</tr>
								<tr>
									<td>12 december '15</td>
									<td>Multimove</td>
								</tr>
								<tr>
									<td>19 december '15</td>
									<td>Multimove</td>
								</tr>
								<tr>
									<td>26 december '15</td>
									<td>Minibad</td>
								</tr>
								<tr>
									<td>2 januari '16</td>
									<td>Multimove</td>
								</tr>
								<tr>
									<td>9 januari '16</td>
									<td>Minibad</td>
								</tr>
								<tr>
									<td>16 januari '16</td>
									<td>Multimove</td>
								</tr>
								<tr>
									<td><strong>23 januari '16</strong></td>
									<td><strong>Geen training</strong></td>
								</tr>
								<tr>
									<td>30 januari '16</td>
									<td>Multimove</td>
								</tr>
								<tr>
									<td>6 februari '16</td>
									<td>Minibad</td>
								</tr>
								<tr>
									<td>13 februari '16</td>
									<td>Multimove</td>
								</tr>
								<tr>
									<td>20 februari '16</td>
									<td>Minibad</td>
								</tr>
								<tr>
									<td>27 februari '16</td>
									<td>Multimove</td>
								</tr>
								<tr>
									<td><strong>5 maart '16</strong></td>
									<td><strong>Geen training</strong></td>
								</tr>
								<tr>
									<td>12 maart '16</td>
									<td>Multimove</td>
								</tr>
								<tr>
									<td>19 maart '16</td>
									<td>Minibad</td>
								</tr>
								<tr>
									<td>26 maart '16</td>
									<td>Multimove</td>
								</tr>
								<tr>
									<td>2 april '16</td>
									<td>Minibad</td>
								</tr>
								<tr>
									<td>9 april '16</td>
									<td>Multimove</td>
								</tr>
								<tr>
									<td>16 april '16</td>
									<td>Minibad</td>
								</tr>
								<tr>
									<td>23 april '16</td>
									<td>Multimove</td>
								</tr>
								<tr>
									<td>30 april '16</td>
									<td>Minibad</td>
								</tr>
								<tr>
									<td>7 mei '16</td>
									<td>Multimove</td>
								</tr>
								<tr>
									<td>14 mei '16</td>
									<td>Minibad</td>
								</tr>
								<tr>
									<td>21 mei '16</td>
									<td>Multimove</td>
								</tr>
								<tr>
									<td>28 mei '16</td>
									<td>Minibad</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="headingTwo">
					<h4 class="panel-title">
						<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
						PBA Jeugdontmoetingen
						</a>
					</h4>
				</div>
				<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
					<div class="panel-body">
						Beste jeugdspelers <br/><br/>
						net zoals vorig jaar kunnen jullie dit seizoen meespelen met het PBA Jeugdcircuit. Dit houdt in dat je tornooien met een officiële ranking kan gaan spelen. In onderstaande kalender zien jullie wanneer de tornooien doorgaan. De uren van de tornooien worden later meegedeeld wanneer wij de officiële uitnodiging ontvangen. <br/>
						Bij de reeks zie je of je kan spelen naargelang jouw leeftijd. Ben je jonger dan 9 jaar, dan speel je in de reeks van de -9 en volgens de aangepaste regels van de minibad.<br/>
						Bij discipline zie je voor welke discipline je kan inschrijven. Bij een mix (dubbel met jongen of meisje) ranking is het aangewezen om zelf een partner op te geven.<br/><br/>
						Wanneer je graag wilt meedoen aan een tornooi, laat je dit weten via {!!Html::mailto('jeugd@bchoogstraten.be', 'jeugd@bchoogstraten.be')!!} zodat wij voor jullie de inschrijving regelen met de organiserende club.
						Hierbij vermeld je je naam, geboortedatum en telefoonnummer. Het inschrijvingsgeld is afhankelijk van wedstrijd tot wedstrijd. De club schiet dit geld voor jullie voor en op het einde van december en op het einde van het seizoen wordt dit doorgerekend aan jullie. Shuttles voor de wedstrijden moeten jullie zelf voorzien. Carpooling wordt door ons aangewezen aangezien de soms verre afstanden, maar wordt niet door Bc Hoogstraten geregeld.<br/><br/>
						Na ieder tornooi ontvang je meestal een klein aandenken zoals een medaille. Op het einde van het seizoen wordt er een officiële prijsuitreiking gehouden met de winnaars van het enkelspel over het hele seizoen.<br/><br/>

						Hopelijk hebben jullie er zin in, wij kijken er alvast naar uit!<br/>
						Veel succes,<br/>
						Sportieve groeten<br/>
						Jullie trainers, het bestuur.
						
						<table class="table table-striped">
							<thead> 
								<tr>
									<th>Datum</th>
									<th>Adres</th>
									<th>Reeks/leeftijd</th>
								</tr>
							</thead>
							
							<tbody>
								<tr> 
									<td>Zaterdag 26 september 2015<br>BD Opslag</td>
									<td><address>Sportpark De Nekker<br>Nekkerspoel-Borcht 19<br>2800 Mechelen</address></td>
									<td>-9/-11/-13/-15/-18</td>
								</tr>
								<tr> 
									<td>Zondag 8 november 2015<br>Noorderwijk</td>
									<td><address>Bloso-centrum Netepark<br>Vorselaarsebaan 60<br>2200 Herentals</address></td>
									<td>-9/-11/-13/-15/-18</td>
								</tr>
								<tr> 
									<td>Zaterdag 21 november 2015<br>Rita Serveert</td>
									<td><address>Sporthal St. Rita College<br>Rompelei<br>2550 Kontich</address></td>
									<td>-9/-11/-13/-15/-18</td>
								</tr>
								<tr> 
									<td>Zondag 25 januari 2016</td>
									<td><address>Gem. Sporthal Zwijndrecht<br>Fortlaan 10<br>2070 Zwijndrecht</address></td>
									<td>-9/-11/-13/-15</td>
								</tr>
								<tr> 
									<td>Zaterdag 30 januari 2016<br>Klein Boom</td>
									<td><address>Sporthal Klein Boom<br>Mechelbaan 604<br>2580 Putte</address></td>
									<td>-9/-11/-13/-15/-18</td>
								</tr>
								<tr> 
									<td>Zaterdag 13 februari 2016<br>Olve</td>
									<td><address>Sporthal Den Willecom<br>Terelstraat 2<br>2650 Edegem</address></td>
									<td>-9/-11/-13/-15/-18</td>
								</tr>
								<tr> 
									<td>Zaterdag 27 februari 2016<br>Baccs</td>
									<td><address>Sporthal Edusport<br>Broeder Frederikstraat 2<br>2170 Merksem</address></td>
									<td>-11/-13/-15/-18</td>
								</tr>
								<tr> 
									<td>Zondag 20 maart 2016<br>De Nekker</td>
									<td><address>Sportpark De Nekker<br>Nekkerspoel-Borcht 19<br>2800 Mechelen</address></td>
									<td>-11/-13/-15/-18</td>
								</tr>
								<tr> 
									<td>Zondag 10 april 2016<br>Plumula</td>
									<td><address>Sportcomplex 't Breeven<br>Barelstraat 111B<br>2880 Bornem</address></td>
									<td>-9/-11/-13/-15/-18</td>
								</tr>
								<tr> 
									<td>Zondag 17 april 2016<br>Waverse</td>
									<td><address>Sporthal Bruultjeshoek<br>Wevervelden 14<br>2861 Onze-Lieve-Vrouw-Waver</address></td>
									<td>-9/-11/-13/-15/-18</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="headingTwo">
					<h4 class="panel-title">
						<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
						JOC
						</a>
					</h4>
				</div>
				<div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
					<div class="panel-body">
						Beste jeugdleden<br/>
						Wij willen jullie ook wat informatie over het JOC (JeugdOntmoetingsCircuit) meegeven.<br/>
						Een JOC is een toernooi tussen de jeugdleden van verschillende clubs in de Kempen. Iedere  club in de buurt organiseert een JOC toernooi en op zondag 31 januari 2016 is er het JOC van Hoogstraten zelf.<br/><br/>
						Vroeger werkten we met een brief die op trainingen werd meegegeven. Omdat dit een heleboel afval met zich meebracht, kiezen we er nu voor om de correspondentie via email te doen. Zorg er daarom voor dat je bij het begin van het seizoen het juiste emailadres opgeeft, zodat je alle brieven op tijd ontvangt. <br/><br/>
						Als jullie op het einde van de reeks aan 6 van de 9 JOC-toernooien hebben deelgenomen wacht jullie een medaille ten teken van je prestatie en inzet.<br/><br/>
						Hieronder de nodige informatie van de verschillende JOC-toernooien dit jaar, zodat jullie deze al kunnen inplannen!<br/>
						
						<table class="table table-striped">
							<thead> 
								<tr>
									<th>Wanneer?</th>
									<th>Waar?</th>
								</tr>
							</thead>
							
							<tbody>
								<tr> 
									<td>Zondag 11 oktober 2015<br>13.00-17.00 uur</td>
									<td><address>Beerse<br>Rerum Novarumlaan 31</address></td>
								</tr>
								<tr> 
									<td>Zaterdag 17 oktober 2015<br>13.00-17.00 uur</td>
									<td><address>Merksplas<br>Sportcentrum 't Hofeind</address></td>
								</tr>
								<tr> 
									<td>Zondag 8 november 2015<br>9.00-13.00 uur</td>
									<td><address>Bouwel<br>Molendreef 8</address></td>
								</tr>
								<tr> 
									<td>Zondag 29 november 2015<br>9.00-13.00 uur</td>
									<td><address>Dessel<br>Brasel z/n</address></td>
								</tr>
								<tr> 
									<td>Zondag 17 januari 2016<br>9.00-13.00 uur</td>
									<td><address>Retie<br>Boesdijkhofstraat 22A</address></td>
								</tr>
								<tr> 
									<td>Zondag 31 januari 2016<br>9.00-12.30 uur</td>
									<td><address>Hoogstraten<br>Gelmelstraat 62</address></td>
								</tr>
								<tr> 
									<td>Zondag 13 maart 2016<br>9.00-13.00 uur</td>
									<td><address>Wijnegem<br>Antwerpsesteenweg 59 2520 Broechem</address></td>
								</tr>
								<tr> 
									<td>Zondag 20 maart 2016<br>9.00-13.00 uur</td>
									<td><address>Zoersel<br>Achterstraat 30</address></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@stop
