@extends('layout.master')

@section('js')
	{!! Html::script('assets/js/isotope.min.js') !!}
	{!! Html::script('assets/js/infinitescroll.min.js') !!}
	<script>
	$(document).ready(function(){
		$('.grid').isotope({
			itemSelector	: '.grid-item'
		});
	});
	</script>
@endsection

@section('title', config('blog.title'))

{{-- Content --}}
@section('content')

<div class="row">
	<div class="col-xs-12">
		<div class="page-header">
			<h1>Volgende evement:</h1>
		</div>
	</div>
</div>
@if (isset($event))
<div class="row">
	<div class="col-xs-12">
			<div class="panel panel-default unstyled-link">
				<div class="panel-heading">
					<h1 class="panel-title">{!! $event->title !!}</h1>
				</div>
				<div class="panel-body">
					<div class="col-xs-12 col-md-6">
						Van:
							<p class="lead">
								<i class="fa fa-calendar-o"></i> {{ $event->begin->format('d/m/y') }} <br>
								<i class="fa fa-clock-o"></i> {{ $event->begin->format('H:i') }}
							</p>
						</p>
					</div>
					<div class="col-xs-12 col-md-6">
						Tot:
							<p class="lead">
								<i class="fa fa-calendar-o"></i> {{ $event->end->format('d/m/y') }} <br>
								<i class="fa fa-clock-o"></i> {{ $event->end->format('H:i') }}
							</p>
					</div>
					<a href="/calendar/{!! $event->slug !!}">Meer info... <i class="fa fa-arrow-circle-o-right"></i></a>
                </div>
			</div>
	</div>
</div>
@else
Er is momenteel geen evenement gepland.
@endif

<div class="row">
	<div class="col-xs-12">
		<div class="page-header">
			<h1>{{ config('blog.title') }}:</h1>
		</div>
	</div>
</div>
<div class="grid row">
	<div class="col-xs-12">
	@foreach ($posts as $post)
			<div class="panel panel-default unstyled-link grid-item">
				<div class="panel-heading">
					<h1 class="panel-title">{!! $post->title !!}</h1>
				</div>
				<div class="panel-body">
					<i class="fa fa-clock-o"></i> {{ $post->updated_at->diffForHumans() }}
					<br>
					@if($post->edited())
						<em><small>(Bewerkt)</small></em>
					@endif
					<div class="pull-right">
						<i class="fa fa-user"></i> {!! $post->user->first_name !!}
					</div>
					<hr>
					<p>
						{!! $post->summary !!}
					</p>
					<hr>
					<a href="/blog/{{ $post->slug }}">Lees meer... <i class="fa fa-arrow-circle-o-right"></i></a>
				</div>
			</div>
	@endforeach
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		{!! $posts->render() !!}
	</div>
</div>
<!-- ./ div -->

@stop
