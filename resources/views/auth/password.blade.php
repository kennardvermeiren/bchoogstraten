@extends('layout.master')

{{-- Content --}}
@section('content')

<div class="page-header">
    <h1>{!! trans('user.password_reset.title') !!}</h1>
</div>

{!! Form::open(array('url' => '/password/email')) !!}
    <div class="row">
        <div class="form-group col-xs-12 col-sm-6 @if ($errors->has('email')) has-error @endif">
            {!! Form::label('email', trans('user.email')) !!}
            {!! Form::text('email', Input::old('email'), array('class' => 'form-control')) !!}
            @if ($errors->has('email')) <span class="help-block">{!! $errors->first('email') !!}</span> @endif
        </div>
    </div>

    <div class="form-actions form-group">
        {!! Form::submit(trans('user.password_reset.btnsubmit'), array('class' => 'btn btn-primary')) !!}
    </div>

{!! Form::close() !!}

@stop