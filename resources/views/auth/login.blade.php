@extends('layout.master')

{{-- Content --}}
@section('content')

<div class="page-header">
    <h1>{!! trans('user.login.title') !!}</h1>
</div>

{!! Form::open(array('url' => '/auth/login')) !!}
    <div class="row">
        <div class="form-group col-xs-12 col-sm-6 @if ($errors->has('email')) has-error @endif">
            {!! Form::label('email', trans('user.email')) !!}
            {!! Form::email('email', Input::old('email'), array('class' => 'form-control')) !!}
            @if ($errors->has('email')) <span class="help-block">{!! $errors->first('email') !!}</span> @endif
        </div>

        <div class="form-group col-xs-12 col-sm-6 @if ($errors->has('password')) has-error @endif">
            {!! Form::label('password', trans('user.password')) !!}
            {!! Form::password('password', array('class' => 'form-control')) !!}
            @if ($errors->has('password')) <span class="help-block">{!! $errors->first('password') !!}</span> @endif
        </div>

        <div class="form-group col-xs-12 col-sm-6">
            <input type="checkbox" name="remember"> Houdt mij aangemeld
        </div>
    </div>

    <div class="form-actions form-group">
        {!! Form::submit(trans('user.login.btnsubmit'), array('class' => 'btn btn-primary')) !!}
        <a class="btn btn-default" href="{{URL::to('password/email')}}">{{{ trans('user.login.btnforgot')}}}</a>
    </div>

{!! Form::close() !!}

@stop