@extends('layout.master')

{{-- Content --}}
@section('content')

<div class="page-header">
    <h1>{!! trans('user.reset.title') !!}</h1>
</div>

{!! Form::open(array('url' => '/password/reset')) !!}
    <input type="hidden" name="token" value="{{ $token }}">
    
    <div class="row">
        <div class="form-group col-xs-12 col-sm-6 @if ($errors->has('email')) has-error @endif">
            {!! Form::label('email', trans('user.email')) !!}
            {!! Form::text('email', Input::old('email'), array('class' => 'form-control')) !!}
            @if ($errors->has('email')) <span class="help-block">{!! $errors->first('email') !!}</span> @endif
        </div>
    </div>

    <div class="row">
        <div class="form-group col-xs-12 col-sm-6 @if ($errors->has('password')) has-error @endif">
            {!! Form::label('password', trans('user.password')) !!}
            {!! Form::password('password', array('class' => 'form-control')) !!}
            @if ($errors->has('password')) <span class="help-block">{!! $errors->first('password') !!}</span> @endif
        </div>

        <div class="form-group col-xs-12 col-sm-6 @if ($errors->has('password_confirmation')) has-error @endif">
            {!! Form::label('password_confirmation', trans('user.password_confirmation')) !!}
            {!! Form::password('password_confirmation', array('class' => 'form-control')) !!}
            @if ($errors->has('password_confirmation')) <span class="help-block">{!! $errors->first('password_confirmation') !!}</span> @endif
        </div>
    </div>

    <div class="form-actions form-group">
        {!! Form::submit('Submit', array('class' => 'btn btn-primary')) !!}
    </div>

{!! Form::close() !!}

@stop