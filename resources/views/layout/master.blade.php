<!DOCTYPE html>
<html lang="nl">
	@include('layout.partials.head')

	<body>
		@if(Request::is('admin/*'))
			@include('layout.partials.admin-navigation')
			@include('layout.partials.admin')
		@else
			@include('layout.partials.site-navigation')
			@include('layout.partials.site')
		@endif
		@include('layout.partials.footer')
		@include('layout.partials.scripts')
	</body>

</html>