<div class="col-xs-12 hidden-xs">
	<h2>In samenwerking met:</h2>
	<img src="{{{ asset('assets/site/sponsors/bad_plus_c.gif') }}}" class="img-responsive"  alt="">
</div>
<hr>
<div class="col-xs-12">
	<h2 class="hidden-xs">Sponsors:</h2>
</div>
<div class="col-xs-12">
	<div id="" class="carousel slide" data-ride="carousel" data-interval="5000">
    <!-- Wrapper for slides -->
    	<div class="carousel-inner" role="listbox">
			<div class="item active">
				<div class="col-xs-3 col-md-12">
					<a target="_blank" href="https://www.facebook.com/jachthoorn">
						<img src="{!!asset('assets/site/sponsors/jachthoorn.png')!!}" class="img-responsive" alt="">
					</a>
				</div>
				<div class="col-xs-3 col-md-12">
					<a target="_blank" href="http://www.cafebizar.be/">
						<img src="{!!asset('assets/site/sponsors/cafebizar.jpg')!!}" class="img-responsive" alt="">
					</a>
				</div>
			</div>
			<div class="item">
				<div class="col-xs-3 col-md-12">
					<a target="_blank" href="http://www.macryan.be/">
						<img src="{!!asset('assets/site/sponsors/macryan.png')!!}" class="img-responsive" alt="">
					</a>
				</div>
				<div class="col-xs-3 col-md-12">
					<a target="_blank" href="http://www.provincieantwerpen.be/">
						<img src="{!!asset('assets/site/sponsors/provincie_antwerpen.png')!!}" class="img-responsive" alt="">
					</a>
				</div>
			</div>
			<div class="item">
				<div class="col-xs-3 col-md-12">
					<a target="_blank" href="http://springertje.be/">
						<img src="{!!asset('assets/site/sponsors/tspringertje.png')!!}" class="img-responsive" alt="">
					</a>
				</div>
				<div class="col-xs-3 col-md-12">
					<a target="_blank" href="http://www.eetkaffeeinthofke.be/">
						<img src="{!!asset('assets/site/sponsors/eetkaffee_inthofke.png')!!}" class="img-responsive" alt="">
					</a>
				</div>
				<div class="col-xs-3 col-md-12">
					<p class="lead"><a href="#"><strong>De Pastinakel<br>Vrijheid 86<br>Hoogstraten</strong></a></p>
				</div>
			</div>
    	</div>
    </div>
</div>


