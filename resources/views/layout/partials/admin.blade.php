<div class="container">
	<div class="row">
		<div class="col-md-12">
			@include('notifications')
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			@yield('content')
		</div>
	</div>
</div>