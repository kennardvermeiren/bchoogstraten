<div class="container">
	<div class="row">
		<div class="col-xs-12">
			@include('notifications')
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-md-9">
			@yield('content')
		</div>
		<div class="col-xs-12 col-md-3">
			@include('layout.partials.side')
		</div>
	</div>
</div>