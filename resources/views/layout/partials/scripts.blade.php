<!-- Javascripts -->
{!! Html::script('https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js') !!}
{!! Html::script('assets/js/vendor.js') !!}
{!! Html::script('assets/js/app.js') !!}

@yield('js')