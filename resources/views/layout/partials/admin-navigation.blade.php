<div class="navbar navbar-default navbar-fixed-top">
	 <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{!!url('/')!!}">
                <img src="{{{ asset('assets/site/logo_bch.png') }}}" alt="" style="max-height:100%;">
            </a>
        </div>
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav">
                <li{{ (Request::is('admin/posts*') ? ' class="active"' : '') }}><a href="{{{ URL::to('admin/posts') }}}"><i class="fa fa-newspaper-o"></i> Blog</a></li>
                <li{{ (Request::is('admin/events*') ? ' class="active"' : '') }}><a href="{{{ URL::to('admin/events') }}}"><i class="fa fa-calendar"></i> Events</a></li>
                <li{{ (Request::is('admin/users*') ? ' class="active"' : '') }}><a href="{{{ URL::to('admin/users') }}}"><i class="fa fa-users"></i> Gebruikers</a></li>
                <li{{ (Request::is('admin/albums*') ? ' class="active"' : '') }}><a href="{{{ URL::to('admin/albums') }}}"><i class="fa fa-picture-o"></i> Albums</a></li>
			</ul>
            <ul class="nav navbar-nav pull-right">
                <li><a href="{{{ URL::to('/') }}}"><i class="fa fa-globe"></i> Terug naar site</a></li>
                <li class="divider-vertical"></li>
                <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-users"></i> {{{ Auth::user()->first_name }}} <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="{{{ URL::to('dashboard') }}}"><i class="fa fa-wrench"></i> Profiel</a></li>
                            <li class="divider"></li>
                            <li><a href="{{{ URL::to('auth/logout') }}}"><i class="fa fa-sign-out"></i> Logout</a></li>
                        </ul>
                </li>
            </ul>
            
			<!-- ./ nav-collapse -->
		</div>
	</div>
</div>
<!-- ./ navbar -->