<div id="footer">
	<div class="container">
		<hr/>
		<div class="row">
			<div class="col-xs-8 text-center">
				<p class="lead">In samenwerking met:</p>
				<div class="row">
					<div class="col-xs-3 col-xs-offset-3">
						<img src="{{{ asset('assets/site/sponsors/badminton_vlaanderen.png') }}}" class="img-responsive"  alt="">
					</div>
					<div class="col-xs-3">
						<img src="{{{ asset('assets/site/sponsors/vlaamse_overheid.png') }}}" class="img-responsive"  alt="">
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-xs-2 col-xs-offset-4">
						<a href="http://www.gsport.be"><strong>G-Sport</strong></a>
					</div>
					<div class="col-xs-2">
						<a href="http://www.apbsport.be"><strong>APB Sport</strong></a>
					</div>
				</div>
            </div>
                <div class="col-xs-4 text-center">
                    <p class="lead">Vindt ons op:</p>
                    <ul class="list-inline">
                        <li>
                            <a href="https://www.facebook.com/badminton.hoogstraten?fref=ts" title="BCHoogstraten op Facebook" target="_blank"><i class="fa fa-facebook fa-fw fa-3x"></i></a>
                        </li>
                    </ul>
			</div>
        </div>
        <hr>
		<div class="row">
            <ul class="list-inline">
            	<li class="text-muted"><a href="http://webmail.bchoogstraten.be/" title="Webmail" target="_blank"><i class="fa fa-envelope-o"></i></a></li>
                <li class="text-muted">Website made by <a href="http://kennard.be" title="kennard.be" target="_blank">kennard.be</a></li>
                <li class="text-muted pull-right">Copyright &copy; bchoogstraten.be {{{date("Y")}}}</li
            </ul>
        </div>
	</div>
</div>