<div class="navbar navbar-default navbar-fixed-top">
	 <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{!!url('/')!!}">
                <img src="{{{ asset('assets/site/logo_bch.png') }}}" alt="" style="max-height:100%;">
            </a>
        </div>
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav">
                <li {{ (Request::is('/blog') ? ' class="active"' : '') }}><a href="{{{ URL::to('') }}}"><i class="fa fa-newspaper-o"></i> Nieuws</a></li>
                <li {{ (Request::is('/calendar') ? ' class="active"' : '') }}><a href="{{{ URL::to('calendar') }}}"><i class="fa fa-calendar"></i> Kalender</a></li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-question"></i> Over <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li {{ (Request::is('about#bchoogstraten') ? 'class="active"' : '') }}><a href="{{{ URL::to('about#bchoogstraten') }}}">BC Hoogstraten</a></li>
                        <li class="divider"></li>
                        <li {{ (Request::is('about#board') ? 'class="active"' : '') }}><a href="{{{ URL::to('about#board') }}}">Bestuur</a></li>
                        <li class="divider"></li>
                        <li {{ (Request::is('about#payment') ? 'class="active"' : '') }}><a href="{{{ URL::to('about#payment') }}}">Betalingsgegevens</a></li>
                        <li class="divider"></li>
                        <li {{ (Request::is('about#prices') ? 'class="active"' : '') }}><a href="{{{ URL::to('about#prices') }}}">Prijzen</a></li>
                        <li class="divider"></li>
                        <li {{ (Request::is('about#adults') ? 'class="active"' : '') }}><a href="{{{ URL::to('about#adults') }}}">Volwassenen</a></li>
                        <li class="divider"></li>
                        <li {{ (Request::is('about#competition') ? 'class="active"' : '') }}><a href="{{{ URL::to('about#competiton') }}}">Competitie</a></li>
                        <li class="divider"></li>
                        <li {{ (Request::is('about#youth') ? 'class="active"' : '') }}><a href="{{{ URL::to('about#youth') }}}">Jeugd</a></li>
                        <li class="divider"></li>
                        <li {{ (Request::is('about#minibad') ? 'class="active"' : '') }}><a href="{{{ URL::to('about#minibad') }}}">Mini-bad</a></li>
                        <li class="divider"></li>
                        <li {{ (Request::is('about#gbad') ? 'class="active"' : '') }}><a href="{{{ URL::to('about#gbad') }}}">G-badminton</a></li>
                    </ul>
                </li>
                <li {{ (Request::is('gallery') ? 'class="active"' : '') }}><a href="{{{ URL::to('gallery') }}}"><i class="fa fa-camera-retro"></i> Galerij</a></li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-info"></i> Info <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li {{ (Request::is('info') ? 'class="active"' : '') }}><a href="{{{ URL::to('info') }}}"><i class="fa fa-info-circle"></i> Algemene Info</a></li>
                        <li class="divider"></li>
                        <li {{ (Request::is('documents') ? 'class="active"' : '') }}><a href="{{{ URL::to('documents') }}}"><i class="fa fa-file-text"></i> Documenten</a></li>
                    </ul>
                </li>
                <li {{ (Request::is('contact') ? 'class="active"' : '') }}><a href="{{{ URL::to('contact') }}}"><i class="fa fa-envelope"></i> Contact</a></li>
			</ul>
            <ul class="nav navbar-nav pull-right">
                @if(Auth::check())
                    @if(Auth::user()->admin === 1)
                    <li><a href="{{{ URL::to('admin') }}}"><i class="fa fa-tachometer"></i> Beheer</a></li>
                    @endif
                    <li class="divider-vertical"></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user"></i> {{{ Auth::user()->first_name }}} <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li {{ (Request::is('dashboard') ? ' class="active"' : '') }}><a href="{{{ URL::to('dashboard') }}}"><i class="fa fa-wrench"></i> Profiel</a></li>
                            <li class="divider"></li>
                            <li><a href="{{{ URL::to('auth/logout') }}}"><i class="fa fa-sign-out"></i> Afmelden</a></li>
                        </ul>
                    </li>
                @else
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user"></i><i class="fa fa-question"></i> {{{ trans('user.guest') }}} <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li {{ (Request::is('auth/login') ? ' class="active"' : '') }}><a href="{{{ URL::to('auth/login') }}}"><i class="fa fa-sign-in"></i> {{{ trans('user.login.title') }}}</a></li>
                            <li class="divider"></li>
                            <li {{ (Request::is('auth/register') ? ' class="active"' : '') }}><a href="{{{ URL::to('auth/register') }}}"><i class="fa fa-pencil"></i> {{{ trans('user.signup.title') }}}</a></li>
                        </ul>
                    </li>
                @endif
            </ul>
            
			<!-- ./ nav-collapse -->
		</div>
	</div>
</div>
<!-- ./ navbar -->