<?php
return [
	'title' => 'Nieuws',
	'posts_per_page' => 6,
	'path' => public_path('assets/blog/'),
	'lowres_width' => 300,
	'hires_width' => 1280
];