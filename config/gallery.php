<?php
return [
	'title' => 'Galerij',
	'albums_per_page' => 5,
	'path' => public_path('assets/gallery/'),
	'lowres_width' => 300,
	'hires_width' => 1280
];